package com.epam.gymcrmsystemspringboot.api;

import com.epam.gymcrmsystemspringboot.dto.trainingType.TrainingTypeResponseDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

/**
 * @author Arpi Khachatryan
 * @date 13.06.2024
 */

public interface TrainingTypeApi {

    @Operation(
            summary = "Get all training types",
            description = "Fetches all available training types from the system."
    )
    @ApiResponses({
            @ApiResponse(
                    responseCode = "200",
                    description = "Successfully fetched the training types.",
                    content = @Content(
                            schema = @Schema(implementation = TrainingTypeResponseDTO.class),
                            mediaType = MediaType.APPLICATION_JSON_VALUE
                    )
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found. The training types do not exist.",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
            )
    })
    ResponseEntity<?> getTrainingTypes();
}
