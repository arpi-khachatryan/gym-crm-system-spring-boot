package com.epam.gymcrmsystemspringboot.api;

import com.epam.gymcrmsystemspringboot.dto.training.TrainingRequestDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

/**
 * @author Arpi Khachatryan
 * @date 13.06.2024
 */

public interface TrainingApi {

    @Operation(
            summary = "Create a new training session",
            description = "Creates a new training session in the system."
    )
    @ApiResponses({
            @ApiResponse(
                    responseCode = "200",
                    description = "Successfully created the training session.",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad request. The request body contains invalid data.",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
            )
    })
    ResponseEntity<?> createTraining(@Valid @RequestBody TrainingRequestDTO trainingRequestDTO, BindingResult bindingResult);
}
