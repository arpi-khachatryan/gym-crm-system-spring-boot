package com.epam.gymcrmsystemspringboot.api;

import com.epam.gymcrmsystemspringboot.dto.general.PasswordChangeDTO;
import com.epam.gymcrmsystemspringboot.dto.general.UserActivationDTO;
import com.epam.gymcrmsystemspringboot.dto.trainer.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Date;

/**
 * @author Arpi Khachatryan
 * @date 13.06.2024
 */

public interface TrainerApi {

    @Operation(
            summary = "Create a new trainer",
            description = "Creates a new trainer in the system."
    )
    @ApiResponses({
            @ApiResponse(
                    responseCode = "201",
                    description = "Successfully created the trainer.",
                    content = @Content(
                            schema = @Schema(implementation = TrainerResponseDTO.class),
                            mediaType = MediaType.APPLICATION_JSON_VALUE
                    )
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad request. The request body contains invalid data.",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
            )
    })
    ResponseEntity<?> createTrainer(@Valid @RequestBody TrainerRequestDTO trainerRequestDTO, BindingResult bindingResult);

    @Operation(
            summary = "Change password for a user",
            description = "Changes the password for a user."
    )
    @ApiResponses({
            @ApiResponse(
                    responseCode = "200",
                    description = "Successfully changed the password.",
                    content = @Content(
                            schema = @Schema(implementation = PasswordChangeDTO.class),
                            mediaType = MediaType.APPLICATION_JSON_VALUE
                    )
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad request. The request body contains invalid data.",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
            )
    })
    ResponseEntity<?> changePassword(@Valid @RequestBody PasswordChangeDTO passwordChangeDTO, BindingResult bindingResult);

    @Operation(
            summary = "Update a trainer",
            description = "Updates a trainer's details."
    )
    @ApiResponses({
            @ApiResponse(
                    responseCode = "200",
                    description = "Successfully updated the trainer.",
                    content = @Content(
                            schema = @Schema(implementation = TrainerUpdateResponseDTO.class),
                            mediaType = MediaType.APPLICATION_JSON_VALUE
                    )
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad request. The request body contains invalid data.",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
            )
    })
    ResponseEntity<?> updateTrainer(@Valid @RequestBody TrainerUpdateRequestDTO trainerUpdateRequestDTO, BindingResult bindingResult);

    @Operation(
            summary = "Activate or deactivate a trainer",
            description = "Activates or deactivates a trainer based on the provided details."
    )
    @ApiResponses({
            @ApiResponse(
                    responseCode = "200",
                    description = "Successfully activated/deactivated the trainer.",
                    content = @Content(
                            schema = @Schema(implementation = String.class),
                            mediaType = MediaType.APPLICATION_JSON_VALUE
                    )
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad request. The request body contains invalid data.",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
            )
    })
    ResponseEntity<?> activateDeactivateTrainer(@Valid @RequestBody UserActivationDTO userActivationDTO, BindingResult bindingResult);

    @Operation(
            summary = "Retrieve a trainer by username",
            description = "Retrieves a trainer's details by their username."
    )
    @ApiResponses({
            @ApiResponse(
                    responseCode = "200",
                    description = "Successfully retrieved the trainer.",
                    content = @Content(
                            schema = @Schema(implementation = TrainerGetResponseDTO.class),
                            mediaType = MediaType.APPLICATION_JSON_VALUE
                    )
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found. The trainer does not exist.",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
            )
    })
    ResponseEntity<?> getTrainer(
            @Parameter(description = "Username of the trainer to retrieve")
            @PathVariable String username);

    @Operation(
            summary = "Search trainings for a trainer",
            description = "Searches for trainings for a trainer based on various criteria."
    )
    @ApiResponses({
            @ApiResponse(
                    responseCode = "200",
                    description = "Successfully found the trainings.",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found. No trainings match the criteria.",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
            )
    })
    ResponseEntity<?> getTrainerTrainings(
            @Parameter(description = "Username of the trainer to retrieve trainings for")
            @PathVariable String username,
            @Parameter(description = "Start date for the training period", example = "2024-01-01")
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date periodFrom,
            @Parameter(description = "End date for the training period", example = "2024-01-31")
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date periodTo,
            @Parameter(description = "Name of the trainee")
            @RequestParam(required = false) String traineeName);
}