package com.epam.gymcrmsystemspringboot.api.controller;

import com.epam.gymcrmsystemspringboot.dto.general.LoginRequestDTO;
import com.epam.gymcrmsystemspringboot.dto.general.LoginResponseDTO;
import com.epam.gymcrmsystemspringboot.service.AuthService;
import com.epam.gymcrmsystemspringboot.util.LoggingUtils;
import com.epam.gymcrmsystemspringboot.validation.ValidationErrorMapping;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author Arpi Khachatryan
 * @date 23.06.2024
 */

@Slf4j
@RestController
public class AuthController {

    private final AuthService authenticationService;

    @Autowired
    public AuthController(AuthService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @PostMapping("/auth/login")
    public ResponseEntity<?> authenticate(@Valid @RequestBody LoginRequestDTO loginRequestDTO, BindingResult bindingResult) {
        String transactionId = LoggingUtils.generateTransactionId();
        log.info("Transaction ID: {}. Request received to authenticate user.", transactionId);
        log.debug("Transaction ID: {}. Endpoint called: POST /auth/login, Request body: {}", transactionId, loginRequestDTO);
        if (bindingResult.hasErrors()) {
            return ValidationErrorMapping.mapValidationErrors(bindingResult);
        }
        LoginResponseDTO responseDTO = authenticationService.authenticate(loginRequestDTO, transactionId);
        log.info("Transaction ID: {}. User authenticated successfully.", transactionId);
        log.debug("Transaction ID: {}. Response: HTTP 200 OK, Authentication Response: {}", transactionId, responseDTO);
        return ResponseEntity.status(HttpStatus.OK).body(responseDTO);
    }
}