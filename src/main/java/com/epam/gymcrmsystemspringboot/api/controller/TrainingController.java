package com.epam.gymcrmsystemspringboot.api.controller;

import com.epam.gymcrmsystemspringboot.api.TrainingApi;
import com.epam.gymcrmsystemspringboot.dto.training.TrainingRequestDTO;
import com.epam.gymcrmsystemspringboot.actuator.GymCrmSystemMetrics;
import com.epam.gymcrmsystemspringboot.service.TrainingService;
import com.epam.gymcrmsystemspringboot.util.LoggingUtils;
import com.epam.gymcrmsystemspringboot.validation.ValidationErrorMapping;
import io.micrometer.core.instrument.Timer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author Arpi Khachatryan
 * @date 18.05.2024
 */

@Slf4j
@RestController
@RequestMapping("/trainings")
public class TrainingController implements TrainingApi {

    private final TrainingService trainingService;

    private final GymCrmSystemMetrics gymCrmSystemMetrics;

    @Autowired
    public TrainingController(TrainingService trainingService, GymCrmSystemMetrics gymCrmSystemMetrics) {
        this.trainingService = trainingService;
        this.gymCrmSystemMetrics = gymCrmSystemMetrics;
    }

    @Override
    @PostMapping
    public ResponseEntity<?> createTraining(@Valid @RequestBody TrainingRequestDTO trainingRequestDTO, BindingResult bindingResult) {
        String transactionId = LoggingUtils.generateTransactionId();
        log.info("Transaction ID: {}. Creating new training: {}", transactionId, trainingRequestDTO);
        log.debug("Transaction ID: {}. Endpoint called: POST /", transactionId);
        Timer.Sample sample = gymCrmSystemMetrics.startTimer("training.creation.timer");
        if (bindingResult.hasErrors()) {
            return ValidationErrorMapping.mapValidationErrors(bindingResult);
        }
        trainingService.createTraining(trainingRequestDTO, transactionId);
        log.info("Transaction ID: {}. Training created successfully", transactionId);
        log.debug("Transaction ID: {}. Response: HTTP 200 OK", transactionId);
        gymCrmSystemMetrics.incrementTrainingCount();
        gymCrmSystemMetrics.stopTimer(sample, "training.creation.timer");
        gymCrmSystemMetrics.decrementActiveSessions();
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}