package com.epam.gymcrmsystemspringboot.api.controller;


import com.epam.gymcrmsystemspringboot.api.TrainingTypeApi;
import com.epam.gymcrmsystemspringboot.dto.trainingType.TrainingTypeResponseDTO;
import com.epam.gymcrmsystemspringboot.actuator.GymCrmSystemMetrics;
import com.epam.gymcrmsystemspringboot.service.TrainingTypeService;
import com.epam.gymcrmsystemspringboot.util.LoggingUtils;
import io.micrometer.core.instrument.Timer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Arpi Khachatryan
 * @date 18.05.2024
 */

@Slf4j
@RestController
@RequestMapping("/trainingTypes")
public class TrainingTypeController implements TrainingTypeApi {

    private final TrainingTypeService trainingTypeService;
    private final GymCrmSystemMetrics gymCrmSystemMetrics;

    @Autowired
    public TrainingTypeController(TrainingTypeService trainingTypeService, GymCrmSystemMetrics gymCrmSystemMetrics) {
        this.trainingTypeService = trainingTypeService;
        this.gymCrmSystemMetrics = gymCrmSystemMetrics;
    }

    @Override
    @GetMapping
    public ResponseEntity<?> getTrainingTypes() {
        String transactionId = LoggingUtils.generateTransactionId();
        log.info("Transaction ID: {}. Fetching all training types", transactionId);
        log.debug("Transaction ID: {}. Endpoint called: GET /", transactionId);
        gymCrmSystemMetrics.incrementTrainingTypeCount();
        Timer.Sample sample = gymCrmSystemMetrics.startTimer("trainingTypes.search.timer");
        List<TrainingTypeResponseDTO> trainingTypes = trainingTypeService.getTrainingTypes(transactionId);
        log.info("Transaction ID: {}. Fetched {} training types successfully", transactionId, trainingTypes.size());
        log.debug("Transaction ID: {}. Response: HTTP 200 OK", transactionId);
        gymCrmSystemMetrics.stopTimer(sample, "trainingTypes.search.timer");
        return ResponseEntity.status(HttpStatus.OK).body(trainingTypes);
    }
}