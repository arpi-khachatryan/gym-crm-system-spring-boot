package com.epam.gymcrmsystemspringboot.api.controller;

import com.epam.gymcrmsystemspringboot.api.TraineeApi;
import com.epam.gymcrmsystemspringboot.dto.general.PasswordChangeDTO;
import com.epam.gymcrmsystemspringboot.dto.general.UserActivationDTO;
import com.epam.gymcrmsystemspringboot.dto.trainee.*;
import com.epam.gymcrmsystemspringboot.dto.trainer.TrainerDTO;
import com.epam.gymcrmsystemspringboot.dto.training.TraineeTrainingResponseDTO;
import com.epam.gymcrmsystemspringboot.actuator.GymCrmSystemMetrics;
import com.epam.gymcrmsystemspringboot.service.TraineeService;
import com.epam.gymcrmsystemspringboot.util.LoggingUtils;
import com.epam.gymcrmsystemspringboot.validation.ValidationErrorMapping;
import io.micrometer.core.instrument.Timer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

/**
 * @author Arpi Khachatryan
 * @date 18.05.2024
 */

@Slf4j
@RestController
@RequestMapping("/trainees")
public class TraineeController implements TraineeApi {

    private final TraineeService traineeService;
    private final GymCrmSystemMetrics gymCrmSystemMetrics;

    @Autowired
    public TraineeController(TraineeService traineeService, GymCrmSystemMetrics gymCrmSystemMetrics) {
        this.traineeService = traineeService;
        this.gymCrmSystemMetrics = gymCrmSystemMetrics;
    }

    @Override
    @PostMapping
    public ResponseEntity<?> createTrainee(@Valid @RequestBody TraineeRequestDTO traineeRequestDTO, BindingResult bindingResult) {
        String transactionId = LoggingUtils.generateTransactionId();
        log.info("Transaction ID: {}. Request received to create a new trainee.", transactionId);
        log.debug("Transaction ID: {}. Endpoint called: POST /trainee, Request body: {}", transactionId, traineeRequestDTO);
        Timer.Sample sample = gymCrmSystemMetrics.startTimer("trainee.creation.timer");
        gymCrmSystemMetrics.incrementTraineeCount();
        if (bindingResult.hasErrors()) {
            return ValidationErrorMapping.mapValidationErrors(bindingResult);
        }
        TraineeResponseDTO createdTrainee = traineeService.createTrainee(traineeRequestDTO, transactionId);
        log.info("Transaction ID: {}. Trainee created successfully.", transactionId);
        log.debug("Transaction ID: {}. Response: HTTP 201 Created, Trainee: {}", transactionId, createdTrainee);
        double responseSize = getResponseSize(createdTrainee);
        gymCrmSystemMetrics.recordResponseSize(responseSize);
        gymCrmSystemMetrics.stopTimer(sample, "trainee.creation.timer");
        return ResponseEntity.status(HttpStatus.CREATED).body(createdTrainee);
    }

    @Override
    @PostMapping("/change-password")
    public ResponseEntity<?> changePassword(@Valid @RequestBody PasswordChangeDTO passwordChangeDTO, BindingResult bindingResult) {
        String transactionId = LoggingUtils.generateTransactionId();
        log.info("Transaction ID: {}. Request received to change password for user with ID: {}", transactionId, passwordChangeDTO.getUserId());
        log.debug("Transaction ID: {}. Endpoint called: POST /change-password, Request body: {}", transactionId, passwordChangeDTO);
        Timer.Sample sample = gymCrmSystemMetrics.startTimer("password.change.timer");
        if (bindingResult.hasErrors()) {
            return ValidationErrorMapping.mapValidationErrors(bindingResult);
        }
        traineeService.changePassword(passwordChangeDTO, transactionId);
        log.info("Transaction ID: {}. Password changed successfully for user with ID: {}", transactionId, passwordChangeDTO.getUserId());
        log.debug("Transaction ID: {}. Response: HTTP 200 OK", transactionId);
        gymCrmSystemMetrics.stopTimer(sample, "password.change.timer");
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @Override
    @PutMapping
    public ResponseEntity<?> updateTrainee(@Valid @RequestBody TraineeUpdateRequestDTO traineeUpdateRequestDTO, BindingResult bindingResult) {
        String transactionId = LoggingUtils.generateTransactionId();
        log.info("Transaction ID: {}. Request received to update trainee: {}", transactionId, traineeUpdateRequestDTO);
        log.debug("Transaction ID: {}. Endpoint called: PUT, Request body: {}", transactionId, traineeUpdateRequestDTO);
        Timer.Sample sample = gymCrmSystemMetrics.startTimer("trainee.update.timer");
        if (bindingResult.hasErrors()) {
            return ValidationErrorMapping.mapValidationErrors(bindingResult);
        }
        TraineeUpdateResponseDTO updatedTrainee = traineeService.updateTrainee(traineeUpdateRequestDTO, transactionId);
        log.info("Transaction ID: {}. Trainee updated successfully: {}", transactionId, updatedTrainee);
        log.debug("Transaction ID: {}. Response: HTTP 200 OK, Updated trainee: {}", transactionId, updatedTrainee);
        gymCrmSystemMetrics.stopTimer(sample, "trainee.update.timer");
        return ResponseEntity.status(HttpStatus.OK).body(updatedTrainee);
    }

    @Override
    @PatchMapping("/activate-deactivate")
    public ResponseEntity<?> activateDeactivateTrainee(@Valid @RequestBody UserActivationDTO userActivationDTO, BindingResult bindingResult) {
        String transactionId = LoggingUtils.generateTransactionId();
        log.info("Transaction ID: {}. Request received to activate/deactivate trainee with username: {}", transactionId, userActivationDTO.getUsername());
        log.debug("Transaction ID: {}. Endpoint called: PATCH, Request body: {}", transactionId, userActivationDTO);
        Timer.Sample sample = gymCrmSystemMetrics.startTimer("trainee.activation.deactivation.timer");
        if (bindingResult.hasErrors()) {
            return ValidationErrorMapping.mapValidationErrors(bindingResult);
        }
        traineeService.activateDeactivateTrainee(userActivationDTO, transactionId);
        log.info("Transaction ID: {}. Trainee with username {} activated/deactivated successfully.", transactionId, userActivationDTO.getUsername());
        log.debug("Transaction ID: {}. Response: HTTP 200 OK, Trainee activated/deactivated successfully.", transactionId);
        gymCrmSystemMetrics.stopTimer(sample, "trainee.activation.deactivation.timer");
        return ResponseEntity.status(HttpStatus.OK).body("Trainee with username " + userActivationDTO.getUsername() + " activated/deactivated successfully.");
    }

    @Override
    @DeleteMapping("/{username}")
    public ResponseEntity<?> deleteTrainee(@PathVariable String username) {
        String transactionId = LoggingUtils.generateTransactionId();
        log.info("Transaction ID: {}. Request received to delete trainee with username: {}", transactionId, username);
        log.debug("Transaction ID: {}. Endpoint called: DELETE, Username: {}", transactionId, username);
        Timer.Sample sample = gymCrmSystemMetrics.startTimer("trainee.deletion.timer");
        traineeService.deleteTrainee(username, transactionId);
        log.info("Transaction ID: {}. Deleted trainee with username: {}", transactionId, username);
        log.debug("Transaction ID: {}. Response: HTTP 200 OK, Trainee deleted successfully.", transactionId);
        gymCrmSystemMetrics.stopTimer(sample, "trainee.deletion.timer");
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @Override
    @GetMapping("/username/{username}")
    public ResponseEntity<TraineeGetResponseDTO> getTrainee(@Valid @PathVariable String username) {
        String transactionId = LoggingUtils.generateTransactionId();
        log.info("Transaction ID: {}. Retrieving trainee with username: {}", transactionId, username);
        log.debug("Transaction ID: {}. Endpoint called: GET, Username: {}", transactionId, username);
        Timer.Sample sample = gymCrmSystemMetrics.startTimer("trainee.retrieve.timer");
        TraineeGetResponseDTO trainee = traineeService.getTrainee(username, transactionId);
        log.info("Transaction ID: {}. Retrieved trainee: {}", transactionId, trainee);
        log.debug("Transaction ID: {}. Response: HTTP 200 OK, Trainee retrieved successfully.", transactionId);
        gymCrmSystemMetrics.stopTimer(sample, "trainee.retrieve.timer");
        return ResponseEntity.status(HttpStatus.OK).body(trainee);
    }

    @Override
    @GetMapping("/{username}/trainings/search")
    public ResponseEntity<?> getTraineeTrainings(
            @PathVariable String username,
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date periodFrom,
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date periodTo,
            @RequestParam(required = false) String trainerName,
            @RequestParam(required = false) String trainingType) {
        String transactionId = LoggingUtils.generateTransactionId();
        log.info("Transaction ID: {}. Searching trainings for trainee with username: {}", transactionId, username);
        log.debug("Transaction ID: {}. Endpoint called: GET, Username: {}, Period from: {}, Period to: {}, Trainer: {}, Training Type: {}", transactionId, username, periodFrom, periodTo, trainerName, trainingType);
        Timer.Sample sample = gymCrmSystemMetrics.startTimer("trainee.trainings.search.timer");
        List<TraineeTrainingResponseDTO> trainings = traineeService.getTraineeTrainingsByCriteria(username, periodFrom, periodTo, trainerName, trainingType, transactionId);
        log.info("Transaction ID: {}. Found {} trainings for trainee with username: {}", transactionId, trainings.size(), username);
        log.debug("Transaction ID: {}. Response: HTTP 200 OK, Trainings retrieved successfully.", transactionId);
        gymCrmSystemMetrics.stopTimer(sample, "trainee.trainings.search.timer");
        return ResponseEntity.status(HttpStatus.OK).body(trainings);
    }

    @Override
    @GetMapping("/{username}/unassigned-trainers")
    public ResponseEntity<List<TrainerDTO>> getUnassignedTrainersByTraineeUsername(@PathVariable String username) {
        String transactionId = LoggingUtils.generateTransactionId();
        log.info("Transaction ID: {}. Retrieving unassigned trainers for trainee with username: {}", transactionId, username);
        log.debug("Transaction ID: {}. Endpoint called: GET, Username: {}", transactionId, username);
        Timer.Sample sample = gymCrmSystemMetrics.startTimer("trainee.unassigned.trainers.retrieve.timer");
        List<TrainerDTO> unassignedTrainers = traineeService.getUnassignedTrainersByTraineeUsername(username, transactionId);
        log.info("Transaction ID: {}. Retrieved {} unassigned trainers for trainee with username: {}", transactionId, unassignedTrainers.size(), username);
        log.debug("Transaction ID: {}. Response: HTTP 200 OK, Unassigned trainers retrieved successfully.", transactionId);
        gymCrmSystemMetrics.stopTimer(sample, "trainee.unassigned.trainers.retrieve.timer");
        return ResponseEntity.status(HttpStatus.OK).body(unassignedTrainers);
    }

    @Override
    @PutMapping("/update-trainers")
    public ResponseEntity<?> updateTraineeTrainers(@Valid @RequestBody TraineeTrainersUpdateRequestDTO traineeTrainersUpdateRequestDTO, BindingResult bindingResult) {
        String transactionId = LoggingUtils.generateTransactionId();
        log.info("Transaction ID: {}. Request received to update trainers for trainee: {}", transactionId, traineeTrainersUpdateRequestDTO.getUsername());
        log.debug("Transaction ID: {}. Endpoint called: PUT /update-trainers, Request body: {}", transactionId, traineeTrainersUpdateRequestDTO);
        Timer.Sample sample = gymCrmSystemMetrics.startTimer("trainee.update.trainers.timer");
        if (bindingResult.hasErrors()) {
            return ValidationErrorMapping.mapValidationErrors(bindingResult);
        }
        List<TrainerDTO> updatedTrainers = traineeService.updateTraineeTrainers(traineeTrainersUpdateRequestDTO, transactionId);
        log.info("Transaction ID: {}. Trainers updated successfully for trainee with username: {}", transactionId, traineeTrainersUpdateRequestDTO.getUsername());
        log.debug("Transaction ID: {}. Response: HTTP 200 OK, Updated trainers: {}", transactionId, updatedTrainers);
        gymCrmSystemMetrics.stopTimer(sample, "trainee.update.trainers.timer");
        return ResponseEntity.status(HttpStatus.OK).body(updatedTrainers);
    }

    private double getResponseSize(TraineeResponseDTO createdTrainee) {
        return createdTrainee.toString().getBytes().length;
    }
}