package com.epam.gymcrmsystemspringboot.api.controller;

import com.epam.gymcrmsystemspringboot.api.TrainerApi;
import com.epam.gymcrmsystemspringboot.dto.general.PasswordChangeDTO;
import com.epam.gymcrmsystemspringboot.dto.general.UserActivationDTO;
import com.epam.gymcrmsystemspringboot.dto.trainer.*;
import com.epam.gymcrmsystemspringboot.dto.training.TrainerTrainingResponseDTO;
import com.epam.gymcrmsystemspringboot.actuator.GymCrmSystemMetrics;
import com.epam.gymcrmsystemspringboot.service.TrainerService;
import com.epam.gymcrmsystemspringboot.util.LoggingUtils;
import com.epam.gymcrmsystemspringboot.validation.ValidationErrorMapping;
import io.micrometer.core.instrument.Timer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

/**
 * @author Arpi Khachatryan
 * @date 18.05.2024
 */

@Slf4j
@RestController
@RequestMapping("/trainers")
public class TrainerController implements TrainerApi {

    private final TrainerService trainerService;

    private final GymCrmSystemMetrics gymCrmSystemMetrics;


    @Autowired
    public TrainerController(TrainerService trainerService, GymCrmSystemMetrics gymCrmSystemMetrics) {
        this.trainerService = trainerService;
        this.gymCrmSystemMetrics = gymCrmSystemMetrics;
    }

    @Override
    @PostMapping
    public ResponseEntity<?> createTrainer(@Valid @RequestBody TrainerRequestDTO trainerRequestDTO, BindingResult bindingResult) {
        String transactionId = LoggingUtils.generateTransactionId();
        log.info("Transaction ID: {}. Request received to create new trainer: {}", transactionId, trainerRequestDTO);
        log.debug("Transaction ID: {}. Endpoint called: POST /createTrainer, Request body: {}", transactionId, trainerRequestDTO);

        Timer.Sample sample = gymCrmSystemMetrics.startTimer("trainer.creation.timer");
        gymCrmSystemMetrics.incrementTrainerCount();

        if (bindingResult.hasErrors()) {
            return ValidationErrorMapping.mapValidationErrors(bindingResult);
        }
        TrainerResponseDTO createdTrainer = trainerService.createTrainer(trainerRequestDTO, transactionId);
        log.info("Transaction ID: {}. Trainer created successfully: {}", transactionId, createdTrainer);
        log.debug("Transaction ID: {}. Response: HTTP 201 Created, Created trainer: {}", transactionId, createdTrainer);

        gymCrmSystemMetrics.stopTimer(sample, "trainer.creation.timer");
        double responseSize = getResponseSize(createdTrainer);
        gymCrmSystemMetrics.recordResponseSize(responseSize);

        return ResponseEntity.status(HttpStatus.CREATED).body(createdTrainer);
    }

    @Override
    @PostMapping("/change-password")
    public ResponseEntity<?> changePassword(@Valid @RequestBody PasswordChangeDTO passwordChangeDTO, BindingResult bindingResult) {
        String transactionId = LoggingUtils.generateTransactionId();
        log.info("Transaction ID: {}. Request received to change password for user with ID: {}", transactionId, passwordChangeDTO.getUserId());
        log.debug("Transaction ID: {}. Endpoint called: POST /change-password, Request body: {}", transactionId, passwordChangeDTO);
        Timer.Sample sample = gymCrmSystemMetrics.startTimer("password.change.timer");
        if (bindingResult.hasErrors()) {
            return ValidationErrorMapping.mapValidationErrors(bindingResult);
        }
        trainerService.changePassword(passwordChangeDTO, transactionId);
        log.info("Transaction ID: {}. Password changed successfully for user with ID: {}", transactionId, passwordChangeDTO.getUserId());
        log.debug("Transaction ID: {}. Response: HTTP 200 OK", transactionId);
        gymCrmSystemMetrics.stopTimer(sample, "password.change.timer");
        return ResponseEntity.ok().build();
    }

    @Override
    @PutMapping
    public ResponseEntity<?> updateTrainer(@Valid @RequestBody TrainerUpdateRequestDTO trainerUpdateRequestDTO, BindingResult bindingResult) {
        String transactionId = LoggingUtils.generateTransactionId();
        log.info("Transaction ID: {}. Request received to update trainer: {}", transactionId, trainerUpdateRequestDTO);
        log.debug("Transaction ID: {}. Endpoint called: PUT /updateTrainer, Request body: {}", transactionId, trainerUpdateRequestDTO);
        Timer.Sample sample = gymCrmSystemMetrics.startTimer("trainer.update.timer");
        if (bindingResult.hasErrors()) {
            return ValidationErrorMapping.mapValidationErrors(bindingResult);
        }
        TrainerUpdateResponseDTO updatedTrainer = trainerService.updateTrainer(trainerUpdateRequestDTO, transactionId);
        log.info("Transaction ID: {}. Trainer updated successfully: {}", transactionId, updatedTrainer);
        log.debug("Transaction ID: {}. Response: HTTP 200 OK", transactionId);
        gymCrmSystemMetrics.stopTimer(sample, "trainer.update.timer");
        return ResponseEntity.status(HttpStatus.OK).body(updatedTrainer);
    }

    @Override
    @PatchMapping("/activate-deactivate")
    public ResponseEntity<?> activateDeactivateTrainer(@Valid @RequestBody UserActivationDTO userActivationDTO, BindingResult bindingResult) {
        String transactionId = LoggingUtils.generateTransactionId();
        log.info("Transaction ID: {}. Request received to activate/deactivate trainer with username: {}", transactionId, userActivationDTO.getUsername());
        log.debug("Transaction ID: {}. Endpoint called: PATCH /activate-deactivate, Request body: {}", transactionId, userActivationDTO);
        Timer.Sample sample = gymCrmSystemMetrics.startTimer("trainer.activation.deactivation.timer");
        if (bindingResult.hasErrors()) {
            return ValidationErrorMapping.mapValidationErrors(bindingResult);
        }
        trainerService.activateDeactivateTrainer(userActivationDTO, transactionId);
        log.info("Transaction ID: {}. Trainer with username {} activated/deactivated successfully.", transactionId, userActivationDTO.getUsername());
        log.debug("Transaction ID: {}. Response: HTTP 200 OK", transactionId);
        gymCrmSystemMetrics.stopTimer(sample, "trainer.activation.deactivation.timer");
        return ResponseEntity.ok().body("Trainer with username " + userActivationDTO.getUsername() + " activated/deactivated successfully.");
    }

    @Override
    @GetMapping("/username/{username}")
    public ResponseEntity<?> getTrainer(@PathVariable String username) {
        String transactionId = LoggingUtils.generateTransactionId();
        log.info("Transaction ID: {}. Retrieving trainer with username: {}", transactionId, username);
        log.debug("Transaction ID: {}. Endpoint called: GET /username/{}", transactionId, username);
        Timer.Sample sample = gymCrmSystemMetrics.startTimer("trainer.retrieve.timer");
        TrainerGetResponseDTO trainer = trainerService.getTrainer(username, transactionId);
        log.info("Transaction ID: {}. Retrieved trainer: {}", transactionId, trainer);
        log.debug("Transaction ID: {}. Response: HTTP 200 OK", transactionId);
        gymCrmSystemMetrics.stopTimer(sample, "trainer.activation.deactivation.timer");
        return ResponseEntity.status(HttpStatus.OK).body(trainer);
    }

    @Override
    @GetMapping("/{username}/trainings/search")
    public ResponseEntity<?> getTrainerTrainings(
            @PathVariable String username,
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date periodFrom,
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date periodTo,
            @RequestParam(required = false) String traineeName) {
        String transactionId = LoggingUtils.generateTransactionId();
        log.info("Transaction ID: {}. Searching trainings for trainer with username: {}", transactionId, username);
        log.debug("Transaction ID: {}. Endpoint called: GET /{}/trainings/search", transactionId, username);
        log.debug("Transaction ID: {}. Query parameters - periodFrom: {}, periodTo: {}, traineeName: {}", transactionId, periodFrom, periodTo, traineeName);
        Timer.Sample sample = gymCrmSystemMetrics.startTimer("trainer.trainees.search.timer");
        List<TrainerTrainingResponseDTO> trainings = trainerService.getTrainerTrainingsByCriteria(username, periodFrom, periodTo, traineeName, transactionId);
        log.info("Transaction ID: {}. Found {} trainings for trainer with username: {}", transactionId, trainings.size(), username);
        log.debug("Transaction ID: {}. Response: HTTP 200 OK", transactionId);
        gymCrmSystemMetrics.stopTimer(sample, "trainer.trainees.search.timer");
        return ResponseEntity.status(HttpStatus.OK).body(trainings);
    }

    private double getResponseSize(TrainerResponseDTO createdTrainer) {
        return createdTrainer.toString().getBytes().length;
    }
}