package com.epam.gymcrmsystemspringboot.mapper;

import com.epam.gymcrmsystemspringboot.dto.trainee.TraineeDTO;
import com.epam.gymcrmsystemspringboot.dto.trainer.TrainerDTO;
import com.epam.gymcrmsystemspringboot.dto.trainer.TrainerGetResponseDTO;
import com.epam.gymcrmsystemspringboot.dto.trainer.TrainerResponseDTO;
import com.epam.gymcrmsystemspringboot.dto.trainer.TrainerUpdateResponseDTO;
import com.epam.gymcrmsystemspringboot.entity.Trainee;
import com.epam.gymcrmsystemspringboot.entity.Trainer;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Arpi Khachatryan
 * @date 09.06.2024
 */

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface TrainerMapper {
    TrainerResponseDTO trainerToTrainerResponseDto(Trainer trainer);

    @Mapping(source = "trainingType.name", target = "specialization")
    @Mapping(source = "trainees", target = "trainees")
    TrainerGetResponseDTO trainerToTrainerGetResponseDTO(Trainer trainer);

    @Mapping(source = "trainingType.name", target = "specialization")
    @Mapping(source = "trainees", target = "trainees")
    TrainerUpdateResponseDTO trainerToTrainerUpdateResponseDTO(Trainer trainer);

    default List<TraineeDTO> mapTrainees(List<Trainee> trainees) {
        return trainees.stream()
                .map(this::traineeToTraineeDTO)
                .collect(Collectors.toList());
    }

    TraineeDTO traineeToTraineeDTO(Trainee trainee);

    @Mapping(source = "trainingType.name", target = "specialization")
    TrainerDTO trainerToTrainerDTO(Trainer trainer);

    List<TrainerDTO> trainerListToTrainerDTOList(List<Trainer> trainers);
}