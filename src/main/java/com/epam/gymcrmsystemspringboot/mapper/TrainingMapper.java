package com.epam.gymcrmsystemspringboot.mapper;

import com.epam.gymcrmsystemspringboot.dto.training.TraineeTrainingResponseDTO;
import com.epam.gymcrmsystemspringboot.dto.training.TrainerTrainingResponseDTO;
import com.epam.gymcrmsystemspringboot.dto.training.TrainingResponseDTO;
import com.epam.gymcrmsystemspringboot.entity.Training;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

import java.util.List;

/**
 * @author Arpi Khachatryan
 * @date 09.06.2024
 */

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface TrainingMapper {
    TrainingResponseDTO trainingToTrainingResponseDTO(Training training);

    @Mappings({
            @Mapping(source = "training.name", target = "trainingName"),
            @Mapping(source = "training.trainingDate", target = "trainingDate", dateFormat = "yyyy-MM-dd"),
            @Mapping(source = "training.trainingType.name", target = "trainingTypeName"),
            @Mapping(source = "training.duration", target = "trainingDuration"),
            @Mapping(source = "training.trainer.username", target = "trainerName")
    })
    TraineeTrainingResponseDTO trainingToTraineeTrainingResponseDTO(Training training);

    List<TraineeTrainingResponseDTO> trainingToTraineeTrainingResponseDTO(List<Training> training);

    @Mappings({
            @Mapping(source = "training.name", target = "trainingName"),
            @Mapping(source = "training.trainingDate", target = "trainingDate", dateFormat = "yyyy-MM-dd"),
            @Mapping(source = "training.trainingType.name", target = "trainingTypeName"),
            @Mapping(source = "training.duration", target = "trainingDuration"),
            @Mapping(source = "training.trainee.username", target = "traineeName")
    })
    TrainerTrainingResponseDTO trainingToTrainerTrainingResponseDTO(Training training);

    List<TrainerTrainingResponseDTO> trainingToTrainerTrainingResponseDTO(List<Training> training);
}