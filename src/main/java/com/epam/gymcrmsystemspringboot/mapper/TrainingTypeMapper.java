package com.epam.gymcrmsystemspringboot.mapper;

import com.epam.gymcrmsystemspringboot.dto.trainingType.TrainingTypeResponseDTO;
import com.epam.gymcrmsystemspringboot.entity.TrainingType;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

/**
 * @author Arpi Khachatryan
 * @date 09.06.2024
 */

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface TrainingTypeMapper {
    List<TrainingTypeResponseDTO> trainingTypeToTrainingTypeDTO(List<TrainingType> trainingTypes);
}