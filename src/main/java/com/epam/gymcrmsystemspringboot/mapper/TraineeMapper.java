package com.epam.gymcrmsystemspringboot.mapper;

import com.epam.gymcrmsystemspringboot.dto.trainee.TraineeGetResponseDTO;
import com.epam.gymcrmsystemspringboot.dto.trainee.TraineeResponseDTO;
import com.epam.gymcrmsystemspringboot.dto.trainee.TraineeUpdateResponseDTO;
import com.epam.gymcrmsystemspringboot.dto.trainer.TrainerDTO;
import com.epam.gymcrmsystemspringboot.entity.Trainee;
import com.epam.gymcrmsystemspringboot.entity.Trainer;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Arpi Khachatryan
 * @date 09.06.2024
 */

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface TraineeMapper {
    TraineeResponseDTO traineeToTraineeResponseDto(Trainee trainee);

    @Mapping(source = "trainee.trainers", target = "trainers")
    TraineeGetResponseDTO traineeToTraineeGetResponseDTO(Trainee trainee);

    @Mapping(source = "trainee.trainers", target = "trainers")
    TraineeUpdateResponseDTO traineeToTraineeUpdateResponseDTO(Trainee trainee);

    default List<TrainerDTO> mapTrainers(Set<Trainer> trainers) {
        return trainers.stream()
                .map(this::trainerToTrainerDTO)
                .collect(Collectors.toList());
    }

    TrainerDTO trainerToTrainerDTO(Trainer trainer);
}