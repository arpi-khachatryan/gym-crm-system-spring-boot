package com.epam.gymcrmsystemspringboot.service;

import com.epam.gymcrmsystemspringboot.dto.general.LoginRequestDTO;
import com.epam.gymcrmsystemspringboot.dto.general.LoginResponseDTO;

/**
 * @author Arpi Khachatryan
 * @date 23.06.2024
 */

public interface AuthService {
    LoginResponseDTO authenticate(LoginRequestDTO loginRequestDTO, String transactionId);
}