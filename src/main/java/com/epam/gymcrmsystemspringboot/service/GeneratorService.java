package com.epam.gymcrmsystemspringboot.service;

/**
 * @author Arpi Khachatryan
 * @date 19.05.2024
 */

public interface GeneratorService {
    String calculateBaseUserName(String firstName, String lastName);

    String generateUniqueUserName(String baseUserName);

    String generateRandomPassword();
}