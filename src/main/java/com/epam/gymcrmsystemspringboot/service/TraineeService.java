package com.epam.gymcrmsystemspringboot.service;

import com.epam.gymcrmsystemspringboot.dto.general.PasswordChangeDTO;
import com.epam.gymcrmsystemspringboot.dto.general.UserActivationDTO;
import com.epam.gymcrmsystemspringboot.dto.trainee.*;
import com.epam.gymcrmsystemspringboot.dto.trainer.TrainerDTO;
import com.epam.gymcrmsystemspringboot.dto.training.TraineeTrainingResponseDTO;

import java.util.Date;
import java.util.List;

/**
 * @author Arpi Khachatryan
 * @date 19.05.2024
 */

public interface TraineeService {
    TraineeResponseDTO createTrainee(TraineeRequestDTO traineeRequestDTO, String transactionId);

    TraineeUpdateResponseDTO updateTrainee(TraineeUpdateRequestDTO traineeUpdateRequestDTO, String transactionId);

    void deleteTrainee(String username, String transactionId);

    TraineeGetResponseDTO getTrainee(String username, String transactionId);

    void changePassword(PasswordChangeDTO passwordChangeDTO, String transactionId);

    void activateDeactivateTrainee(UserActivationDTO userActivationDTO, String transactionId);

    List<TraineeTrainingResponseDTO> getTraineeTrainingsByCriteria(String username, Date periodFrom, Date periodTo, String trainerName, String trainingTyp, String transactionId);

    List<TrainerDTO> getUnassignedTrainersByTraineeUsername(String username, String transactionId);

    List<TrainerDTO> updateTraineeTrainers(TraineeTrainersUpdateRequestDTO traineeTrainersUpdateRequestDTO, String transactionId);
}