package com.epam.gymcrmsystemspringboot.service.serviceImpl;

import com.epam.gymcrmsystemspringboot.dto.trainingType.TrainingTypeResponseDTO;
import com.epam.gymcrmsystemspringboot.entity.TrainingType;
import com.epam.gymcrmsystemspringboot.exception.ResourceNotFoundException;
import com.epam.gymcrmsystemspringboot.mapper.TrainingTypeMapper;
import com.epam.gymcrmsystemspringboot.repository.TrainingTypeRepository;
import com.epam.gymcrmsystemspringboot.service.TrainingTypeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Arpi Khachatryan
 * @date 19.05.2024
 */

@Slf4j
@Service
public class TrainingTypeServiceImpl implements TrainingTypeService {

    @Autowired
    private TrainingTypeRepository trainingTypeRepository;

    @Autowired
    private TrainingTypeMapper trainingTypeMapper;

    /**
     * Retrieves all existing training types.
     * Steps:
     * 1. Retrieve all training type entities from the database.
     * 2. If training types are found, log the retrieval and return the list of DTOs representing the training types.
     * 3. If no training types are found, log a warning and return an empty list.
     * 4. If an error occurs during retrieval, log the error and return an empty list.
     *
     * @return A list of DTOs representing the retrieved training types, or an empty list if none are found or an error occurs.
     * @throws ResourceNotFoundException if the training types is not found.
     */
    @Override
    @Transactional(readOnly = true)
    public List<TrainingTypeResponseDTO> getTrainingTypes(String transactionId) {
        log.info("Transaction ID: {}. Retrieving all training types.", transactionId);

        try {
            List<TrainingType> trainingTypes = trainingTypeRepository.findAll();
            if (trainingTypes.isEmpty()) {
                log.warn("Transaction ID: {}. No training types found.", transactionId);
                throw new ResourceNotFoundException("No training types found.");
            } else {
                List<TrainingTypeResponseDTO> trainingTypeDTOs = trainingTypeMapper.trainingTypeToTrainingTypeDTO(trainingTypes);
                log.info("Transaction ID: {}. Retrieved training types: {}", transactionId, trainingTypeDTOs);
                return trainingTypeDTOs;
            }
        } catch (Exception e) {
            log.error("Transaction ID: {}. Error retrieving training types: {}", transactionId, e.getMessage());
            log.debug("Transaction ID: {}. Response: HTTP 404 Not Found, Error message: {}", transactionId, e.getMessage());
            throw new ResourceNotFoundException("Error retrieving training types.", e);
        }
    }
}
