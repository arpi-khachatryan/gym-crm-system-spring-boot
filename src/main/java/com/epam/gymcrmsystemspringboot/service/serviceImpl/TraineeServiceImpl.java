package com.epam.gymcrmsystemspringboot.service.serviceImpl;

import com.epam.gymcrmsystemspringboot.dto.general.PasswordChangeDTO;
import com.epam.gymcrmsystemspringboot.dto.general.UserActivationDTO;
import com.epam.gymcrmsystemspringboot.dto.trainee.*;
import com.epam.gymcrmsystemspringboot.dto.trainer.TrainerDTO;
import com.epam.gymcrmsystemspringboot.dto.trainer.TrainerUpdateDTO;
import com.epam.gymcrmsystemspringboot.dto.training.TraineeTrainingResponseDTO;
import com.epam.gymcrmsystemspringboot.entity.Trainee;
import com.epam.gymcrmsystemspringboot.entity.Trainer;
import com.epam.gymcrmsystemspringboot.entity.Training;
import com.epam.gymcrmsystemspringboot.exception.*;
import com.epam.gymcrmsystemspringboot.mapper.TraineeMapper;
import com.epam.gymcrmsystemspringboot.mapper.TrainerMapper;
import com.epam.gymcrmsystemspringboot.mapper.TrainingMapper;
import com.epam.gymcrmsystemspringboot.repository.TraineeRepository;
import com.epam.gymcrmsystemspringboot.repository.TrainerRepository;
import com.epam.gymcrmsystemspringboot.repository.TrainingRepository;
import com.epam.gymcrmsystemspringboot.service.GeneratorService;
import com.epam.gymcrmsystemspringboot.service.TraineeService;
import com.epam.gymcrmsystemspringboot.specification.TraineeTrainingSpecifications;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Arpi Khachatryan
 * @date 19.05.2024
 */

@Slf4j
@Service
public class TraineeServiceImpl implements TraineeService {

    @Autowired
    private GeneratorService generatorService;

    @Autowired
    private TraineeRepository traineeRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private TrainerRepository trainerRepository;

    @Autowired
    private TrainingRepository trainingRepository;

    @Autowired
    private TrainerMapper trainerMapper;

    @Autowired
    private TraineeMapper traineeMapper;

    @Autowired
    private TrainingMapper trainingMapper;

    /**
     * Creates a new trainee.
     * Steps:
     * 1. Generate a unique username and password for the trainee.
     * 2. Hash the password for security before saving it.
     * 3. Create a Trainee entity, associating it with the provided address and date of birth.
     * 4. If a trainer ID is specified, the trainee is associated with the corresponding trainer.
     * 5. If training IDs are provided, the trainee is associated with the specified trainings.
     * 6. Save the Trainee entity to the database.
     *
     * @param traineeRequestDTO The DTO containing trainee creation data.
     * @return The created TraineeResponseDTO.
     * @throws ResourceCreationException if an error occurred while creating trainee.
     */
    @Override
    @Transactional
    public TraineeResponseDTO createTrainee(TraineeRequestDTO traineeRequestDTO, String transactionId) {
        log.info("Transaction ID: {}. Creating new trainee: {}", transactionId, traineeRequestDTO);

        try {
            String baseUsername = generatorService.calculateBaseUserName(traineeRequestDTO.getFirstName(), traineeRequestDTO.getLastName());
            String username = generatorService.generateUniqueUserName(baseUsername);
            String rawPassword = generatorService.generateRandomPassword();
            String hashedPassword = passwordEncoder.encode(rawPassword);

            Trainee trainee = new Trainee(
                    null,
                    traineeRequestDTO.getFirstName(),
                    traineeRequestDTO.getLastName(),
                    username,
                    hashedPassword,
                    true,
                    traineeRequestDTO.getDateOfBirth(),
                    traineeRequestDTO.getAddress()
            );

            if (traineeRequestDTO.getTrainerId() != null) {
                Optional<Trainer> trainerOptional = trainerRepository.findById(traineeRequestDTO.getTrainerId());
                trainerOptional.ifPresent(trainer -> trainee.getTrainers().add(trainer));
            }

            if (traineeRequestDTO.getTrainingIds() != null && !traineeRequestDTO.getTrainingIds().isEmpty()) {
                List<Training> trainings = trainingRepository.findAllById(traineeRequestDTO.getTrainingIds());
                trainee.getTrainings().addAll(trainings);
            }

            Trainee createdTrainee = traineeRepository.save(trainee);
            log.info("Transaction ID: {}. Created trainee: {}", transactionId, createdTrainee);
            log.info("Transaction ID: {}. Generated password for new trainee: {}", transactionId, rawPassword);

            TraineeResponseDTO responseDTO = traineeMapper.traineeToTraineeResponseDto(createdTrainee);
            responseDTO.setPassword(rawPassword);
            return responseDTO;
        } catch (Exception e) {
            log.error("Transaction ID: {}. Error creating trainee: {}", transactionId, e.getMessage());
            log.debug("Transaction ID: {}. Response: HTTP 400 Bad Request, Error message: {}", transactionId, e.getMessage());
            throw new ResourceCreationException("Error creating trainee", e);
        }
    }

    /**
     * Changes the password for a trainee.
     * Steps:
     * 1. Find the trainee by their ID.
     * 2. If the trainee is found, proceed with the password change process.
     * 3. Check if the provided old password matches the current password and if the new password is different from the old one.
     * 4. If the conditions are met, update the password.
     *
     * @param passwordChangeDTO The DTO containing information for password change.
     * @throws PasswordChangeException if an error occurred while changing the password.
     */
    @Override
    @Transactional
    public void changePassword(PasswordChangeDTO passwordChangeDTO, String transactionId) {
        log.info("Transaction ID: {}. Changing password for user with ID: {}", transactionId, passwordChangeDTO.getUserId());

        try {
            Optional<Trainee> optionalTrainee = traineeRepository.findById(passwordChangeDTO.getUserId());
            if (optionalTrainee.isPresent()) {
                Trainee trainee = optionalTrainee.get();
                String currentPassword = trainee.getPassword();
                String oldPassword = passwordChangeDTO.getOldPassword();
                String newPassword = passwordChangeDTO.getNewPassword();

                if (currentPassword.equals(oldPassword) && !currentPassword.equals(newPassword)) {
                    trainee.setPassword(newPassword);
                    traineeRepository.save(trainee);

                    log.info("Transaction ID: {}. Password changed successfully for user with ID: {}", transactionId, passwordChangeDTO.getUserId());
                } else {
                    log.warn("Transaction ID: {}. Password change conditions not met.", transactionId);
                    throw new PasswordChangeException("Password change conditions not met. Either old password doesn't match or new password is the same as old password.");
                }
            } else {
                log.warn("Transaction ID: {}. Trainee with ID {} not found.", transactionId, passwordChangeDTO.getUserId());
                throw new PasswordChangeException("Trainee not found.");
            }
        } catch (Exception e) {
            log.error("Transaction ID: {}. Error changing password: {}", transactionId, e.getMessage());
            log.debug("Transaction ID: {}. Response: HTTP 400 Bad Request, Error message: {}", transactionId, e.getMessage());
            throw new PasswordChangeException("An error occurred while changing the password.", e);
        }
    }

    /**
     * Updates an existing trainee.
     * Steps:
     * 1. Checks if the trainee ID is valid.
     * 2. Retrieves the existing trainee.
     * 3. Generates and updates the username if necessary.
     * 4. Saves the updated trainee information.
     *
     * @param traineeUpdateRequestDTO The DTO containing updated trainee information.
     * @return The updated trainee entity.
     * @throws ResourceUpdateException an error occurred while updating the trainee.
     */
    @Override
    @Transactional
    public TraineeUpdateResponseDTO updateTrainee(TraineeUpdateRequestDTO traineeUpdateRequestDTO, String transactionId) {
        log.info("Transaction ID: {}. Updating trainee: {}", transactionId, traineeUpdateRequestDTO);

        try {
            Optional<Trainee> traineeOptional = traineeRepository.findByUsername(traineeUpdateRequestDTO.getUsername());
            if (traineeOptional.isEmpty()) {
                log.warn("Transaction ID: {}. Trainee with username {} not found", transactionId, traineeUpdateRequestDTO.getUsername());
                throw new ResourceUpdateException("Trainee not found.");
            }

            Trainee existingTrainee = traineeOptional.get();

            existingTrainee.setFirstName(traineeUpdateRequestDTO.getFirstName());
            existingTrainee.setLastName(traineeUpdateRequestDTO.getLastName());
            existingTrainee.setAddress(traineeUpdateRequestDTO.getAddress());
            existingTrainee.setDateOfBirth(traineeUpdateRequestDTO.getDateOfBirth());
            existingTrainee.setActive(traineeUpdateRequestDTO.getIsActive());

            Trainee updatedTrainee = traineeRepository.save(existingTrainee);
            TraineeUpdateResponseDTO responseDTO = traineeMapper.traineeToTraineeUpdateResponseDTO(updatedTrainee);

            log.info("Transaction ID: {}. Updated trainee: {}", transactionId, updatedTrainee);
            return responseDTO;
        } catch (Exception e) {
            log.error("Transaction ID: {}. Error updating trainee: {}", transactionId, e.getMessage());
            log.debug("Transaction ID: {}. Response: HTTP 400 Bad Request, Error message: {}", transactionId, e.getMessage());
            throw new ResourceUpdateException("An error occurred while updating the trainee.", e);
        }
    }

    /**
     * Activates or deactivates a trainee.
     * Steps:
     * 1. Find the trainee by their username.
     * 2. If the trainee is found, update their active status based on the provided boolean.
     * 3. Save the updated trainee entity.
     *
     * @param userActivationDTO The DTO containing the trainee's username and active status.
     * @throws ActivationDeactivationException an error occurred while activating/deactivating the trainee.
     */
    @Override
    @Transactional
    public void activateDeactivateTrainee(UserActivationDTO userActivationDTO, String transactionId) {
        log.info("Transaction ID: {}. Activating/Deactivating trainee with username: {}", transactionId, userActivationDTO.getUsername());

        try {
            Optional<Trainee> traineeOptional = traineeRepository.findByUsername(userActivationDTO.getUsername());
            if (traineeOptional.isPresent()) {
                Trainee trainee = traineeOptional.get();
                trainee.setActive(userActivationDTO.getIsActive());
                traineeRepository.save(trainee);
                log.info("Transaction ID: {}. Trainee with username {} {}.", transactionId, userActivationDTO.getUsername(), userActivationDTO.getIsActive() ? "activated" : "deactivated");
            } else {
                log.warn("Transaction ID: {}. Trainee with username {} not found.", transactionId, userActivationDTO.getUsername());
                throw new ActivationDeactivationException("Trainee not found.");
            }
        } catch (Exception e) {
            log.error("Transaction ID: {}. Error activating/deactivating trainee: {}", transactionId, e.getMessage());
            log.debug("Transaction ID: {}. Response: HTTP 400 Bad Request, Error message: {}", transactionId, e.getMessage());
            throw new ActivationDeactivationException("An error occurred while activating/deactivating the trainee.", e);
        }
    }

    /**
     * Deletes a trainee by their username.
     * Steps:
     * 1. Retrieve the trainee entity by their username along with associated trainings.
     * 2. If the trainee is not found, log a warning and return false.
     * 3. Delete the trainee entity.
     *
     * @param username The username of the trainee to delete.
     * @throws ResourceDeletionException an error occurred while deleting the trainee.
     */
    @Override
    @Transactional
    public void deleteTrainee(String username, String transactionId) {
        log.info("Transaction ID: {}. Deleting trainee with username: {}", transactionId, username);

        try {
            Optional<Trainee> traineeOptional = traineeRepository.findByUsername(username);
            if (traineeOptional.isEmpty()) {
                log.warn("Transaction ID: {}. Trainee with username {} not found", transactionId, username);
                throw new ResourceDeletionException("Trainee not found.");
            }

            Trainee trainee = traineeOptional.get();
            for (Trainer trainer : trainee.getTrainers()) {
                trainer.getTrainees().remove(trainee);
            }

            traineeRepository.delete(trainee);
            log.info("Transaction ID: {}. Trainee with username {} deleted successfully", transactionId, username);
        } catch (Exception e) {
            log.error("Transaction ID: {}. Error deleting trainee: {}", transactionId, e.getMessage());
            log.debug("Transaction ID: {}. Response: HTTP 400 Bad Request, Error message: {}", transactionId, e.getMessage());
            throw new ResourceDeletionException("An error occurred while deleting the trainee.", e);
        }
    }

    /**
     * Retrieves an existing trainee based on the provided username.
     * Steps:
     * 1. Log the retrieval process with the provided username.
     * 2. Retrieve the trainee entity by its username.
     * 3. If the trainee is found, log the retrieval and return the trainee entity.
     * 4. If the trainee is not found, log a warning and return null.
     *
     * @param username The username associated with the trainee to retrieve.
     * @return The retrieved Trainee entity.
     * @throws ResourceNotFoundException if the trainee with the specified username is not found.
     */
    @Override
    @Transactional(readOnly = true)
    public TraineeGetResponseDTO getTrainee(String username, String transactionId) {
        log.info("Transaction ID: {}. Retrieving trainee with username: {}", transactionId, username);

        try {
            Optional<Trainee> traineeOptional = traineeRepository.findByUsernameWithTrainers(username);
            if (traineeOptional.isPresent()) {
                Trainee trainee = traineeOptional.get();
                log.info("Transaction ID: {}. Retrieved trainee: {}", transactionId, trainee);
                return traineeMapper.traineeToTraineeGetResponseDTO(trainee);
            } else {
                log.warn("Transaction ID: {}. Trainee with username {} not found", transactionId, username);
                throw new ResourceNotFoundException("Trainee not found.");
            }
        } catch (Exception e) {
            log.error("Transaction ID: {}. Error retrieving trainee: {}", transactionId, e.getMessage());
            log.debug("Transaction ID: {}. Response: HTTP 404 Not Found, Error message: {}", transactionId, e.getMessage());
            throw new ResourceNotFoundException("An error occurred while retrieving the trainee.", e);
        }
    }

    /**
     * Retrieves trainings based on the provided search criteria for a specific trainee.
     * Steps:
     * 1. Construct a Specification to filter trainings based on the trainee's username and date range.
     * 2. Optionally, add additional filtering criteria based on trainer name and training type name.
     * 3. Retrieve trainings matching the specification.
     * 4. Convert the matching trainings to DTOs.
     * 5. Return the list of DTOs representing the trainings.
     *
     * @param traineeUsername The username of the trainee for whom to retrieve trainings.
     * @param periodFrom      The start date of the period to filter trainings.
     * @param periodTo        The end date of the period to filter trainings.
     * @param trainerName     The name of the trainer to filter trainings.
     * @param trainingType    The type of training to filter trainings.
     * @return A list of DTOs representing the trainings matching the search criteria.
     * @throws ResourceNotFoundException if an error occurred while retrieving trainee trainings.
     */
    @Override
    @Transactional(readOnly = true)
    public List<TraineeTrainingResponseDTO> getTraineeTrainingsByCriteria(String traineeUsername, Date periodFrom, Date periodTo, String trainerName, String trainingType, String transactionId) {
        log.info("Transaction ID: {}. Retrieving trainings for trainee with username: {}", transactionId, traineeUsername);
        try {
            Specification<Training> specification = Specification.where(TraineeTrainingSpecifications.hasTraineeUsername(traineeUsername));
            if (periodFrom != null || periodTo != null) {
                log.debug("Transaction ID: {}. Adding date range filter: periodFrom: {}, periodTo: {}", transactionId, periodFrom, periodTo);
                specification = specification.and(TraineeTrainingSpecifications.trainingDateBetween(periodFrom, periodTo));
            }

            if (trainerName != null && !trainerName.isEmpty()) {
                log.debug("Transaction ID: {}. Adding trainer filter: {}", transactionId, trainerName);
                specification = specification.and(TraineeTrainingSpecifications.hasTrainerUsername(trainerName));
            }

            if (trainingType != null && !trainingType.isEmpty()) {
                log.debug("Transaction ID: {}. Adding training type filter: {}", transactionId, trainingType);
                specification = specification.and(TraineeTrainingSpecifications.hasTrainingTypeName(trainingType));
            }

            List<Training> trainings = trainingRepository.findAll(specification);
            log.debug("Transaction ID: {}. Found {} trainings matching the search criteria", transactionId, trainings.size());

            if (trainings.isEmpty()) {
                throw new ResourceNotFoundException("An error occurred while retrieving trainee trainings.");
            }

            List<TraineeTrainingResponseDTO> trainingDTOs = trainingMapper.trainingToTraineeTrainingResponseDTO(trainings);
            log.info("Transaction ID: {}. Retrieved {} trainings for trainee with username: {}", transactionId, trainingDTOs.size(), traineeUsername);
            return trainingDTOs;
        } catch (Exception e) {
            log.error("Transaction ID: {}. Error retrieving trainee trainings: {}", transactionId, e.getMessage(), e);
            log.debug("Transaction ID: {}. Response: HTTP 404 Not Found, Error message: {}", transactionId, e.getMessage());
            throw new ResourceNotFoundException("An error occurred while retrieving trainee trainings.", e);
        }
    }

    /**
     * Retrieves unassigned trainers for a specific trainee by the trainee's username.
     * Steps:
     * 1. Retrieve unassigned trainers for the specified trainee from the database.
     * 2. Convert the retrieved trainers to DTOs.
     * 3. Return the list of DTOs representing the unassigned trainers.
     *
     * @param traineeUsername The username of the trainee.
     * @return A list of DTOs representing the unassigned trainers.
     * @throws ResourceNotFoundException if an error occurred while retrieving unassigned trainers.
     */
    @Override
    @Transactional(readOnly = true)
    public List<TrainerDTO> getUnassignedTrainersByTraineeUsername(String traineeUsername, String transactionId) {
        log.info("Transaction ID: {}. Retrieving unassigned trainers for trainee with username: {}", transactionId, traineeUsername);
        try {
            List<Trainer> unassignedTrainers = trainerRepository.findUnassignedTrainersByTraineeUsername(traineeUsername);
            List<TrainerDTO> trainerDTOs = trainerMapper.trainerListToTrainerDTOList(unassignedTrainers);

            log.info("Transaction ID: {}. Retrieved {} unassigned trainers for trainee with username: {}", transactionId, trainerDTOs.size(), traineeUsername);
            return trainerDTOs;
        } catch (Exception e) {
            log.error("Transaction ID: {}. Error retrieving unassigned trainers: {}", transactionId, e.getMessage(), e);
            log.debug("Transaction ID: {}. Response: HTTP 404 Not Found, Error message: {}", transactionId, e.getMessage());
            throw new ResourceNotFoundException("An error occurred while retrieving unassigned trainers.", e);
        }
    }

    /**
     * Updates the list of trainers assigned to a trainee.
     * Steps:
     * 1. Retrieve the trainee by their username from the database.
     * 2. If the trainee is found, update their list of trainers with the provided list of trainer IDs.
     * 3. Save the updated trainee entity.
     *
     * @param traineeTrainersUpdateRequestDTO The DTO containing the username of the trainee and the list of trainer usernames to assign.
     * @return A list of DTOs representing the updated list of trainers assigned to the trainee.
     * @throws ResourceUpdateException if an error occurred while updating trainers list.
     */
    @Override
    @Transactional
    public List<TrainerDTO> updateTraineeTrainers(TraineeTrainersUpdateRequestDTO traineeTrainersUpdateRequestDTO, String transactionId) {
        String traineeUsername = traineeTrainersUpdateRequestDTO.getUsername();
        log.info("Transaction ID: {}. Updating trainers for trainee with username: {}", transactionId, traineeUsername);
        try {
            Optional<Trainee> traineeOptional = traineeRepository.findByUsername(traineeUsername);
            if (traineeOptional.isPresent()) {
                Trainee trainee = traineeOptional.get();

                List<String> trainerUsernames = traineeTrainersUpdateRequestDTO.getTrainerUpdateDTOList().stream()
                        .map(TrainerUpdateDTO::getUsername)
                        .collect(Collectors.toList());

                List<Trainer> trainers = trainerRepository.findAllByUsernameIn(trainerUsernames);
                if (!trainers.isEmpty()) {
                    trainee.getTrainers().clear();
                    trainee.getTrainers().addAll(trainers);
                    traineeRepository.save(trainee);
                    log.info("Transaction ID: {}. Updated trainers list for trainee with username: {}", transactionId, traineeUsername);
                    return trainerMapper.trainerListToTrainerDTOList(trainee.getTrainers());
                } else {
                    log.warn("Transaction ID: {}. No trainers found for the provided usernames", transactionId);
                    throw new ResourceNotFoundException("No trainers found for the provided usernames.");
                }
            } else {
                log.warn("Transaction ID: {}. Trainee with username {} not found", transactionId, traineeUsername);
                throw new ResourceNotFoundException("Trainee not found.");
            }
        } catch (Exception e) {
            log.error("Transaction ID: {}. Error updating trainers list: {}", transactionId, e.getMessage(), e);
            log.debug("Transaction ID: {}. Response: HTTP 400 Bad Request, Error message: {}", transactionId, e.getMessage());
            throw new ResourceUpdateException("An error occurred while updating trainers list.", e);
        }
    }
}
