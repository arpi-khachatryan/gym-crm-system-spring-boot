package com.epam.gymcrmsystemspringboot.service.serviceImpl;

import com.epam.gymcrmsystemspringboot.dto.training.TrainingRequestDTO;
import com.epam.gymcrmsystemspringboot.entity.Trainee;
import com.epam.gymcrmsystemspringboot.entity.Trainer;
import com.epam.gymcrmsystemspringboot.entity.Training;
import com.epam.gymcrmsystemspringboot.entity.TrainingType;
import com.epam.gymcrmsystemspringboot.exception.ResourceCreationException;
import com.epam.gymcrmsystemspringboot.repository.TraineeRepository;
import com.epam.gymcrmsystemspringboot.repository.TrainerRepository;
import com.epam.gymcrmsystemspringboot.repository.TrainingRepository;
import com.epam.gymcrmsystemspringboot.repository.TrainingTypeRepository;
import com.epam.gymcrmsystemspringboot.service.TrainingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * @author Arpi Khachatryan
 * @date 19.05.2024
 */

@Slf4j
@Service
public class TrainingServiceImpl implements TrainingService {

    @Autowired
    private TrainingRepository trainingRepository;

    @Autowired
    private TraineeRepository traineeRepository;

    @Autowired
    private TrainerRepository trainerRepository;

    @Autowired
    private TrainingTypeRepository trainingTypeRepository;

    /**
     * Creates a new training.
     * Steps:
     * 1. Retrieve the Trainee entity by username from the database.
     * 2. Retrieve the Trainer entity by username from the database.
     * 3. Retrieve the TrainingType entity by name from the database.
     * 4. Check if a training with the same name, date, duration, trainee, and trainer already exists.
     * 5. If the training does not exist, create a new Training entity with the provided data.
     * 6. Save the Training entity to the database.
     *
     * @param trainingRequestDTO The DTO containing the information required to create the training.
     * @throws ResourceCreationException if an error occurred while creating training.
     */
    @Override
    @Transactional
    public void createTraining(TrainingRequestDTO trainingRequestDTO, String transactionId) {
        log.info("Transaction ID: {}. Creating training: {}", transactionId, trainingRequestDTO);

        try {
            Trainee trainee = traineeRepository.findByUsername(trainingRequestDTO.getTraineeUsername())
                    .orElseThrow(() -> {
                        log.error("Transaction ID: {}. Trainee not found for username: {}", transactionId, trainingRequestDTO.getTraineeUsername());
                        return new ResourceCreationException("Trainee not found");
                    });

            Trainer trainer = trainerRepository.findByUsername(trainingRequestDTO.getTrainerUsername())
                    .orElseThrow(() -> {
                        log.error("Transaction ID: {}. Trainer not found for username: {}", transactionId, trainingRequestDTO.getTrainerUsername());
                        return new ResourceCreationException("Trainer not found");
                    });

            TrainingType trainingType = trainingTypeRepository.findByName(trainingRequestDTO.getTrainingTypeName());
            if (trainingType == null) {
                log.error("Transaction ID: {}. Training type not found: {}", transactionId, trainingRequestDTO.getTrainingTypeName());
                throw new ResourceCreationException("Training type not found");
            }

            Optional<Training> existingTraining = trainingRepository.findByNameDateDurationAndTraineeTrainerUsernamesAndTrainingTypeName(
                    trainingRequestDTO.getTrainingName(),
                    trainingRequestDTO.getTrainingDate(),
                    trainingRequestDTO.getTrainingDuration(),
                    trainingRequestDTO.getTraineeUsername(),
                    trainingRequestDTO.getTrainerUsername(),
                    trainingRequestDTO.getTrainingTypeName()
            );
            if (existingTraining.isPresent()) {
                log.error("Transaction ID: {}. A training with the same name, date, duration, trainee, and trainer already exists", transactionId);
                throw new ResourceCreationException("A training with the same name, date, duration, trainee, and trainer already exists");
            }

            Training training = Training.builder()
                    .name(trainingRequestDTO.getTrainingName())
                    .trainingDate(trainingRequestDTO.getTrainingDate())
                    .duration(trainingRequestDTO.getTrainingDuration())
                    .trainee(trainee)
                    .trainer(trainer)
                    .trainingType(trainingType)
                    .build();

            trainingRepository.save(training);
            log.info("Transaction ID: {}. Training created successfully: {}", transactionId, training);
        } catch (Exception e) {
            log.error("Transaction ID: {}. Error occurred while creating training", transactionId, e);
            log.debug("Transaction ID: {}. Response: HTTP 400 Bad Request, Error message: {}", transactionId, e.getMessage());
            throw new ResourceCreationException("An error occurred while creating training");
        }
    }
}