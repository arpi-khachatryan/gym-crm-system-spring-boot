package com.epam.gymcrmsystemspringboot.service.serviceImpl;

import com.epam.gymcrmsystemspringboot.dto.general.LoginRequestDTO;
import com.epam.gymcrmsystemspringboot.dto.general.LoginResponseDTO;
import com.epam.gymcrmsystemspringboot.entity.Trainee;
import com.epam.gymcrmsystemspringboot.entity.Trainer;
import com.epam.gymcrmsystemspringboot.exception.AuthenticationException;
import com.epam.gymcrmsystemspringboot.repository.TraineeRepository;
import com.epam.gymcrmsystemspringboot.repository.TrainerRepository;
import com.epam.gymcrmsystemspringboot.security.LoginAttemptService;
import com.epam.gymcrmsystemspringboot.service.AuthService;
import com.epam.gymcrmsystemspringboot.util.JwtTokenUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * @author Arpi Khachatryan
 * @date 23.06.2024
 */

@Slf4j
@Service
public class AuthServiceImpl implements AuthService {

    @Autowired
    private TraineeRepository traineeRepository;

    @Autowired
    private TrainerRepository trainerRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private LoginAttemptService loginAttemptService;

    @Autowired
    private HttpServletRequest request;

    @Override
    public LoginResponseDTO authenticate(LoginRequestDTO loginRequestDTO, String transactionId) {
        log.info("Transaction ID: {}. Authenticating user with username {}", transactionId, loginRequestDTO.getUsername());

        String clientIP = getClientIP();

        if (loginAttemptService.isBlocked(clientIP)) {
            log.error("Transaction ID: {}. IP {} is blocked due to too many unsuccessful login attempts", transactionId, clientIP);
            throw new AuthenticationException("Too many unsuccessful login attempts. Please try again later.");
        }

        Trainee trainee = traineeRepository.findByUsername(loginRequestDTO.getUsername())
                .orElse(null);

        if (trainee == null) {
            Optional<Trainer> trainerOptional = trainerRepository.findByUsername(loginRequestDTO.getUsername());

            if (trainerOptional.isEmpty()) {
                log.warn("Transaction ID: {}. User with username {} not found", transactionId, loginRequestDTO.getUsername());
                loginAttemptService.loginFailed(clientIP);
                throw new AuthenticationException("Invalid credentials");
            }

            if (passwordEncoder.matches(loginRequestDTO.getPassword(), trainerOptional.get().getPassword())) {
                log.info("Transaction ID: {}. Trainer with username {} authenticated successfully", transactionId, trainerOptional.get().getUsername());
                loginAttemptService.loginSucceeded(clientIP);
                String token = jwtTokenUtil.generateToken(trainerOptional.get());
                log.debug("Transaction ID: {}. Generated JWT token: {}", transactionId, token);
                return LoginResponseDTO.builder()
                        .token(token)
                        .build();
            } else {
                log.warn("Transaction ID: {}. Invalid password for trainer with username {}", transactionId, trainerOptional.get().getUsername());
                loginAttemptService.loginFailed(clientIP);
                throw new AuthenticationException("Invalid credentials");
            }
        } else {
            if (passwordEncoder.matches(loginRequestDTO.getPassword(), trainee.getPassword())) {
                log.info("Transaction ID: {}. Trainee with username {} authenticated successfully", transactionId, trainee.getUsername());
                loginAttemptService.loginSucceeded(clientIP);
                String token = jwtTokenUtil.generateToken(trainee);
                log.debug("Transaction ID: {}. Generated JWT token: {}", transactionId, token);
                return LoginResponseDTO.builder()
                        .token(token)
                        .build();
            } else {
                log.warn("Transaction ID: {}. Invalid password for trainee with username {}", transactionId, trainee.getUsername());
                loginAttemptService.loginFailed(clientIP);
                throw new AuthenticationException("Invalid credentials");
            }
        }
    }

    private String getClientIP() {
        String xfHeader = request.getHeader("X-Forwarded-For");
        if (xfHeader == null || xfHeader.isEmpty()) {
            return request.getRemoteAddr();
        }
        return xfHeader.split(",")[0];
    }
}