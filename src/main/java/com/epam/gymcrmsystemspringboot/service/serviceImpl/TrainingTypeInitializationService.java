package com.epam.gymcrmsystemspringboot.service.serviceImpl;

import com.epam.gymcrmsystemspringboot.entity.TrainingType;
import com.epam.gymcrmsystemspringboot.repository.TrainingTypeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;

/**
 * @author Arpi Khachatryan
 * @date 19.05.2024
 */

@Slf4j
@Service
public class TrainingTypeInitializationService {

    @Autowired
    private TrainingTypeRepository trainingTypeRepository;

    @PostConstruct
    public void init() {
        createTrainingTypes();
    }

    /**
     * Creates default training types if they don't exist already.
     * Steps:
     * 1. Check if each default training type exists in the repository.
     * 2. If a training type doesn't exist, create it and log the creation.
     * 3. If a training type already exists, log its presence.
     */
    private void createTrainingTypes() {
        List<String> defaultTrainingTypes = Arrays.asList("Cardio", "Fitness", "Yoga", "Pilates");
        for (String typeName : defaultTrainingTypes) {
            if (!trainingTypeRepository.existsByName(typeName)) {
                log.info("Creating training type: {}", typeName);
                TrainingType trainingType = TrainingType.builder()
                        .name(typeName)
                        .build();
                trainingTypeRepository.save(trainingType);
                log.info("Training type '{}' created successfully.", typeName);
            } else {
                log.info("Training type '{}' already exists.", typeName);
            }
        }
    }
}
