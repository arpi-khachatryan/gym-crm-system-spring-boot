package com.epam.gymcrmsystemspringboot.service.serviceImpl;

import com.epam.gymcrmsystemspringboot.dto.general.PasswordChangeDTO;
import com.epam.gymcrmsystemspringboot.dto.general.UserActivationDTO;
import com.epam.gymcrmsystemspringboot.dto.trainer.*;
import com.epam.gymcrmsystemspringboot.dto.training.TrainerTrainingResponseDTO;
import com.epam.gymcrmsystemspringboot.entity.Trainee;
import com.epam.gymcrmsystemspringboot.entity.Trainer;
import com.epam.gymcrmsystemspringboot.entity.Training;
import com.epam.gymcrmsystemspringboot.entity.TrainingType;
import com.epam.gymcrmsystemspringboot.exception.*;
import com.epam.gymcrmsystemspringboot.mapper.TrainerMapper;
import com.epam.gymcrmsystemspringboot.mapper.TrainingMapper;
import com.epam.gymcrmsystemspringboot.repository.TraineeRepository;
import com.epam.gymcrmsystemspringboot.repository.TrainerRepository;
import com.epam.gymcrmsystemspringboot.repository.TrainingRepository;
import com.epam.gymcrmsystemspringboot.repository.TrainingTypeRepository;
import com.epam.gymcrmsystemspringboot.service.GeneratorService;
import com.epam.gymcrmsystemspringboot.service.TrainerService;
import com.epam.gymcrmsystemspringboot.specification.TrainerTrainingSpecifications;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @author Arpi Khachatryan
 * @date 19.05.2024
 */

@Slf4j
@Service
public class TrainerServiceImpl implements TrainerService {

    @Autowired
    private GeneratorService generatorService;

    @Autowired
    private TrainerRepository trainerRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private TrainingTypeRepository trainingTypeRepository;

    @Autowired
    private TrainingRepository trainingRepository;

    @Autowired
    private TraineeRepository traineeRepository;

    @Autowired
    private TrainerMapper trainerMapper;

    @Autowired
    private TrainingMapper trainingMapper;

    /**
     * Creates a new trainer.
     * Steps:
     * 1. Generate a unique username and password for the trainer.
     * 2. Hash the password for security before saving it.
     * 3. Retrieve the TrainingType entity based on the provided training type name.
     * 4. If the TrainingType does not exist, create a new one and save it to the database.
     * 5. Create the Trainer entity and associate it with the retrieved or created TrainingType.
     * 6. Associate trainees with the trainer if provided.
     * 7. Save the Trainer entity to the database.
     *
     * @param trainerRequestDTO The DTO containing information for creating a new trainer.
     * @return The newly created Trainer entity.
     * @throws ResourceCreationException if an error occurred while creating the trainer.
     */
    @Override
    @Transactional
    public TrainerResponseDTO createTrainer(TrainerRequestDTO trainerRequestDTO, String transactionId) {
        log.info("Transaction ID: {}. Creating new trainer: {}", transactionId, trainerRequestDTO);

        try {
            String baseUsername = generatorService.calculateBaseUserName(trainerRequestDTO.getFirstName(), trainerRequestDTO.getLastName());
            String username = generatorService.generateUniqueUserName(baseUsername);
            String rawPassword = generatorService.generateRandomPassword();
            String hashedPassword = passwordEncoder.encode(rawPassword);

            TrainingType trainingType = trainingTypeRepository.findByName(trainerRequestDTO.getTrainingTypeName());
            if (trainingType == null) {
                trainingType = new TrainingType();
                trainingType.setName(trainerRequestDTO.getTrainingTypeName());
                trainingType = trainingTypeRepository.save(trainingType);
            }

            Trainer trainer = new Trainer(
                    null,
                    trainerRequestDTO.getFirstName(),
                    trainerRequestDTO.getLastName(),
                    username,
                    hashedPassword,
                    true,
                    trainingType,
                    new ArrayList<>()
            );

            if (trainerRequestDTO.getTraineeIds() != null && !trainerRequestDTO.getTraineeIds().isEmpty()) {
                List<Trainee> trainees = new ArrayList<>();
                for (Long traineeId : trainerRequestDTO.getTraineeIds()) {
                    Optional<Trainee> traineeOptional = traineeRepository.findById(traineeId);
                    traineeOptional.ifPresent(trainees::add);
                }
                trainer.setTrainees(trainees);
            }

            Trainer createdTrainer = trainerRepository.save(trainer);
            log.info("Transaction ID: {}. Created trainer: {}", transactionId, createdTrainer);

            TrainerResponseDTO responseDTO = trainerMapper.trainerToTrainerResponseDto(createdTrainer);
            responseDTO.setPassword(rawPassword);
            return responseDTO;
        } catch (Exception e) {
            log.error("Transaction ID: {}. Error creating trainer: {}", transactionId, e.getMessage(), e);
            log.debug("Transaction ID: {}. Response: HTTP 400 Bad Request, Error message: {}", transactionId, e.getMessage());
            throw new ResourceCreationException("An error occurred while creating the trainer.", e);
        }
    }

    /**
     * Changes the password for a trainer.
     * Steps:
     * 1. Find the trainer by their ID.
     * 2. If the trainer is found, proceed with the password change process.
     * 3. Check if the provided old password matches the current password and if the new password is different from the old one.
     * 4. If the conditions are met, update the password.
     *
     * @param passwordChangeDTO The DTO containing information for password change.
     * @throws PasswordChangeException if an error occurred while changing the password.
     */
    @Override
    @Transactional
    public void changePassword(PasswordChangeDTO passwordChangeDTO, String transactionId) {
        log.info("Transaction ID: {}. Changing password for user with ID: {}", transactionId, passwordChangeDTO.getUserId());

        try {
            Optional<Trainer> optionalTrainer = trainerRepository.findById(passwordChangeDTO.getUserId());
            if (optionalTrainer.isPresent()) {
                Trainer trainer = optionalTrainer.get();
                String currentPassword = trainer.getPassword();
                String oldPassword = passwordChangeDTO.getOldPassword();
                String newPassword = passwordChangeDTO.getNewPassword();

                if (currentPassword.equals(oldPassword) && !currentPassword.equals(newPassword)) {
                    trainer.setPassword(newPassword);
                    trainerRepository.save(trainer);

                    log.info("Transaction ID: {}. Password changed successfully for user with ID: {}", transactionId, passwordChangeDTO.getUserId());
                } else {
                    log.warn("Transaction ID: {}. Password change conditions not met.", transactionId);
                    throw new PasswordChangeException("Password change conditions not met. Either old password doesn't match or new password is the same as old password..");
                }
            } else {
                log.warn("Transaction ID: {}. Trainer with ID {} not found.", transactionId, passwordChangeDTO.getUserId());
                throw new PasswordChangeException("Trainer not found.");
            }
        } catch (Exception e) {
            log.error("Transaction ID: {}. Error changing password: {}", transactionId, e.getMessage());
            log.debug("Transaction ID: {}. Response: HTTP 400 Bad Request, Error message: {}", transactionId, e.getMessage());
            throw new PasswordChangeException("An error occurred while changing the password.", e);
        }
    }

    /**
     * Updates an existing trainer.
     * Steps:
     * 1. Checks if the trainer ID is valid.
     * 2. Retrieves the existing trainer.
     * 3. Updates the trainer's information.
     * 4. Saves the updated trainer information.
     *
     * @param trainerUpdateRequestDTO The DTO containing updated trainer information.
     * @return The updated trainer entity.
     * @throws ResourceUpdateException an error occurred while updating the trainer.
     */
    @Override
    @Transactional
    public TrainerUpdateResponseDTO updateTrainer(TrainerUpdateRequestDTO trainerUpdateRequestDTO, String transactionId) {
        log.info("Transaction ID: {}. Updating trainer: {}", transactionId, trainerUpdateRequestDTO);

        try {
            Optional<Trainer> trainerOptional = trainerRepository.findByUsername(trainerUpdateRequestDTO.getUsername());
            if (trainerOptional.isEmpty()) {
                log.warn("Transaction ID: {}. Trainer with ID {} not found", transactionId, trainerUpdateRequestDTO.getUsername());
                throw new ResourceUpdateException("Trainer not found.");
            }

            Trainer existingTrainer = trainerOptional.get();
            existingTrainer.setFirstName(trainerUpdateRequestDTO.getFirstName());
            existingTrainer.setLastName(trainerUpdateRequestDTO.getLastName());
            existingTrainer.setActive(trainerUpdateRequestDTO.getIsActive());

            Trainer updatedTrainer = trainerRepository.save(existingTrainer);
            log.info("Transaction ID: {}. Updated trainer: {}", transactionId, updatedTrainer);
            return trainerMapper.trainerToTrainerUpdateResponseDTO(updatedTrainer);
        } catch (Exception e) {
            log.error("Transaction ID: {}. Error updating trainer: {}", transactionId, e.getMessage());
            log.debug("Transaction ID: {}. Response: HTTP 400 Bad Request, Error message: {}", transactionId, e.getMessage());
            throw new ResourceUpdateException("An error occurred while updating the trainer.", e);
        }
    }

    /**
     * Activates or deactivates a trainer.
     * Steps:
     * 1. Find the trainer by their username.
     * 2. If the trainer is found, update their active status based on the provided boolean.
     * 3. Save the updated trainer entity.
     *
     * @param userActivationDTO The DTO containing updated trainer information.
     * @throws ActivationDeactivationException an error occurred while activating/deactivating the trainer.
     */
    @Override
    @Transactional
    public void activateDeactivateTrainer(UserActivationDTO userActivationDTO, String transactionId) {
        log.info("Transaction ID: {}. Activating/Deactivating trainer with username: {}", transactionId, userActivationDTO.getUsername());

        try {
            Optional<Trainer> trainerOptional = trainerRepository.findByUsername(userActivationDTO.getUsername());
            if (trainerOptional.isPresent()) {
                Trainer trainer = trainerOptional.get();
                trainer.setActive(userActivationDTO.getIsActive());
                trainerRepository.save(trainer);
                log.info("Transaction ID: {}. Trainer with username {} {}.", transactionId, userActivationDTO.getUsername(), userActivationDTO.getIsActive() ? "activated" : "deactivated");
            } else {
                log.warn("Transaction ID: {}. Trainer with username {} not found.", transactionId, userActivationDTO.getUsername());
                throw new ActivationDeactivationException("Trainer not found.");
            }
        } catch (Exception e) {
            log.error("Transaction ID: {}. Error activating/deactivating trainer: {}", transactionId, e.getMessage());
            log.debug("Transaction ID: {}. Response: HTTP 400 Bad Request, Error message: {}", transactionId, e.getMessage());
            throw new ActivationDeactivationException("An error occurred while activating/deactivating the trainer.", e);
        }
    }

    /**
     * Retrieves an existing trainer based on the provided username.
     * Steps:
     * 1. Log the retrieval process with the provided username.
     * 2. Retrieve the trainer entity by its username.
     * 3. If the trainer is found, log the retrieval and return the trainer entity.
     * 4. If the trainer is not found, log a warning and return null.
     *
     * @param username The username associated with the trainer to retrieve.
     * @return The retrieved Trainer entity.
     * @throws ResourceDeletionException an error occurred while deleting the trainer.
     */
    @Override
    @Transactional(readOnly = true)
    public TrainerGetResponseDTO getTrainer(String username, String transactionId) {
        log.info("Transaction ID: {}. Retrieving trainer with username: {}", transactionId, username);

        try {
            Optional<Trainer> trainerOptional = trainerRepository.findByUsername(username);
            if (trainerOptional.isPresent()) {
                Trainer trainer = trainerOptional.get();
                log.info("Transaction ID: {}. Retrieved trainer: {}", transactionId, trainer);
                return trainerMapper.trainerToTrainerGetResponseDTO(trainer);
            } else {
                log.warn("Transaction ID: {}. Trainer with username {} not found", transactionId, username);
                throw new ResourceNotFoundException("Trainer not found.");
            }
        } catch (Exception e) {
            log.error("Transaction ID: {}. Error retrieving trainer: {}", transactionId, e.getMessage());
            log.debug("Transaction ID: {}. Response: HTTP 404 Not Found, Error message: {}", transactionId, e.getMessage());
            throw new ResourceNotFoundException("An error occurred while retrieving the trainer.", e);
        }
    }

    /**
     * Retrieves trainings for a trainer based on the provided search criteria.
     * Steps:
     * 1. Construct a list of Specifications to filter trainings based on the trainer's username, date range, and trainee name.
     * 2. Combine the Specifications into a single Specification using conjunction (AND).
     * 3. Retrieve trainings matching the combined Specification.
     * 4. Convert the matching trainings to DTOs.
     * 5. Return the list of DTOs representing the trainings.
     *
     * @param trainerUsername The username of the trainer.
     * @param periodFrom      The start date of the period to filter trainings.
     * @param periodTo        The end date of the period to filter trainings.
     * @param traineeName     The name of the trainee to filter trainings.
     * @return A list of DTOs representing the trainings matching the search criteria.
     * @throws ResourceNotFoundException if an error occurred while retrieving trainer trainings.
     */
    @Override
    @Transactional(readOnly = true)
    public List<TrainerTrainingResponseDTO> getTrainerTrainingsByCriteria(String trainerUsername, Date periodFrom, Date periodTo, String traineeName, String transactionId) {
        log.info("Transaction ID: {}. Retrieving trainings for trainer with username: {}", transactionId, trainerUsername);
        try {
            Specification<Training> specification = Specification.where(TrainerTrainingSpecifications.hasTrainerUsername(trainerUsername));

            if (periodFrom != null || periodTo != null) {
                log.debug("Transaction ID: {}. Adding date range filter: periodFrom: {}, periodTo: {}", transactionId, periodFrom, periodTo);
                specification = specification.and(TrainerTrainingSpecifications.trainingDateBetween(periodFrom, periodTo));
            }

            if (traineeName != null && !traineeName.isEmpty()) {
                log.debug("Transaction ID: {}. Adding trainee filter: {}", transactionId, traineeName);
                specification = specification.and(TrainerTrainingSpecifications.hasTraineeName(traineeName));
            }

            List<Training> trainings = trainingRepository.findAll(specification);
            log.debug("Transaction ID: {}. Found {} trainings matching the search criteria", transactionId, trainings.size());

            if (trainings.isEmpty()) {
                throw new ResourceNotFoundException("An error occurred while retrieving trainer trainings.");
            }

            List<TrainerTrainingResponseDTO> trainingDTOs = trainingMapper.trainingToTrainerTrainingResponseDTO(trainings);
            log.info("Transaction ID: {}. Retrieved {} trainings for trainer with username: {}", transactionId, trainingDTOs.size(), trainerUsername);
            return trainingDTOs;
        } catch (Exception e) {
            log.error("Transaction ID: {}. Error retrieving trainer trainings: {}", transactionId, e.getMessage(), e);
            log.debug("Transaction ID: {}. Response: HTTP 404 Not Found, Error message: {}", transactionId, e.getMessage());
            throw new ResourceNotFoundException("An error occurred while retrieving trainer trainings.", e);
        }
    }
}