package com.epam.gymcrmsystemspringboot.service.serviceImpl;

import com.epam.gymcrmsystemspringboot.repository.TraineeRepository;
import com.epam.gymcrmsystemspringboot.repository.TrainerRepository;
import com.epam.gymcrmsystemspringboot.service.GeneratorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.stream.Collectors;

/**
 * @author Arpi Khachatryan
 * @date 19.05.2024
 */

@Slf4j
@Service
public class GeneratorServiceImpl implements GeneratorService {

    @Autowired
    private TraineeRepository traineeRepository;

    @Autowired
    private TrainerRepository trainerRepository;

    /**
     * Calculates the base username using the user's first name and last name.
     * Steps:
     * 1. Concatenate the first name and last name with a dot in between.
     * 2. Return the concatenated string as the base username.
     *
     * @param firstName The first name of the user.
     * @param lastName  The last name of the user.
     * @return The calculated base username.
     */
    public String calculateBaseUserName(String firstName, String lastName) {
        String baseUserName = firstName + "." + lastName;
        log.debug("Calculated base username: {}", baseUserName);
        return baseUserName;
    }

    /**
     * Generates a unique username based on the provided base username.
     * Steps:
     * 1. Initialize the username with the base username.
     * 2. Append a numerical suffix to the base username until a unique username is found.
     * 3. Return the generated unique username.
     *
     * @param baseUsername The base username to start with.
     * @return The generated unique username.
     */
    public String generateUniqueUserName(String baseUsername) {
        String username = baseUsername;
        int suffix = 1;

        while (traineeRepository.existsByUsername(username) || trainerRepository.existsByUsername(username)) {
            username = baseUsername + suffix;
            suffix++;
        }
        log.debug("Generated unique username: {}", username);
        return username;
    }

    /**
     * Generates a random password of 10 characters.
     * Steps:
     * 1. Generate random integers within the ASCII range for printable characters.
     * 2. Convert each integer to its corresponding character.
     * 3. Concatenate the characters to form the password.
     *
     * @return The generated random password.
     */
    public String generateRandomPassword() {
        String password = new SecureRandom().ints(10, 33, 122)
                .mapToObj(x -> String.valueOf((char) x))
                .collect(Collectors.joining());
        log.debug("Generated random password: {}", password);
        return password;
    }
}