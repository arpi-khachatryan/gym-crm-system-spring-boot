package com.epam.gymcrmsystemspringboot.service;

import com.epam.gymcrmsystemspringboot.dto.general.PasswordChangeDTO;
import com.epam.gymcrmsystemspringboot.dto.general.UserActivationDTO;
import com.epam.gymcrmsystemspringboot.dto.trainer.*;
import com.epam.gymcrmsystemspringboot.dto.training.TrainerTrainingResponseDTO;

import java.util.Date;
import java.util.List;

/**
 * @author Arpi Khachatryan
 * @date 19.05.2024
 */

public interface TrainerService {
    TrainerResponseDTO createTrainer(TrainerRequestDTO trainerRequestDTO, String transactionId);

    TrainerUpdateResponseDTO updateTrainer(TrainerUpdateRequestDTO trainerUpdateRequestDTO, String transactionId);

    TrainerGetResponseDTO getTrainer(String username, String transactionId);

    void changePassword(PasswordChangeDTO passwordChangeDTO, String transactionId);

    void activateDeactivateTrainer(UserActivationDTO userActivationDTO, String transactionId);

    List<TrainerTrainingResponseDTO> getTrainerTrainingsByCriteria(String username, Date periodFrom, Date periodTo, String traineeName, String transactionId);
}