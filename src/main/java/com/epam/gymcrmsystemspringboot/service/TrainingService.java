package com.epam.gymcrmsystemspringboot.service;

import com.epam.gymcrmsystemspringboot.dto.training.TrainingRequestDTO;

/**
 * @author Arpi Khachatryan
 * @date 19.05.2024
 */

public interface TrainingService {
    void createTraining(TrainingRequestDTO trainingRequestDTO, String transactionId);
}