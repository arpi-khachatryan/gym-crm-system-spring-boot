package com.epam.gymcrmsystemspringboot.service;

import com.epam.gymcrmsystemspringboot.dto.trainingType.TrainingTypeResponseDTO;

import java.util.List;

/**
 * @author Arpi Khachatryan
 * @date 19.05.2024
 */

public interface TrainingTypeService {
    List<TrainingTypeResponseDTO> getTrainingTypes(String transactionId);
}