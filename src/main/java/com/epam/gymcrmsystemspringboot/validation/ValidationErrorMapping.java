package com.epam.gymcrmsystemspringboot.validation;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Arpi Khachatryan
 * @date 23.05.2024
 */

@Slf4j
@Component
public class ValidationErrorMapping {

    /**
     * Maps validation errors from BindingResult to a ResponseEntity with error details.
     *
     * @param bindingResult The BindingResult containing validation errors.
     * @return ResponseEntity with error details.
     */
    public static ResponseEntity<?> mapValidationErrors(BindingResult bindingResult) {
        Map<String, String> errors = new HashMap<>();
        for (FieldError error : bindingResult.getFieldErrors()) {
            errors.put(error.getField(), error.getDefaultMessage());
            log.error("Validation error for field '{}' - Message: '{}'", error.getField(), error.getDefaultMessage());
        }
        Map<String, Object> errorModel = new HashMap<>();
        errorModel.put("message", "Validation failed. Please check your data and try again.");
        errorModel.put("errors", errors);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorModel);
    }
}