package com.epam.gymcrmsystemspringboot.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Arpi Khachatryan
 * @date 25.06.2024
 */

@Slf4j
@Component
public class GymCrmSystemAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

    @Autowired
    private MessageSource messages;

    @Autowired
    private LocaleResolver localeResolver;

    @Autowired
    private LoginAttemptService loginAttemptService;

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException {
        String clientIP = getClientIP(request);
        log.info("Authentication failure for IP: {}", clientIP);
        loginAttemptService.loginFailed(clientIP);

        if (loginAttemptService.isBlocked(clientIP)) {
            String errorMessage = messages.getMessage("auth.message.blocked", null, localeResolver.resolveLocale(request));
            response.sendError(HttpServletResponse.SC_FORBIDDEN, errorMessage);
            log.warn("User with IP {} is blocked due to too many login attempts.", clientIP);
        } else {
            String errorMessage = messages.getMessage("message.badCredentials", null, localeResolver.resolveLocale(request));
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, errorMessage);
            log.warn("Authentication failed for IP: {}. Bad credentials provided.", clientIP);
        }
    }

    private String getClientIP(HttpServletRequest request) {
        String xfHeader = request.getHeader("X-Forwarded-For");
        if (xfHeader == null || xfHeader.isEmpty() || !xfHeader.contains(request.getRemoteAddr())) {
            return request.getRemoteAddr();
        }
        return xfHeader.split(",")[0];
    }
}