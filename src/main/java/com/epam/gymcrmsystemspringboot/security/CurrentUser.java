package com.epam.gymcrmsystemspringboot.security;

import com.epam.gymcrmsystemspringboot.entity.User;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collections;

/**
 * @author Arpi Khachatryan
 * @date 23.06.2024
 */

@Slf4j
@Getter
public class CurrentUser extends org.springframework.security.core.userdetails.User {

    private final User user;

    public CurrentUser(User user) {
        super(user.getUsername(), user.getPassword(), user.isActive(), true, true, true, Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER")));
        this.user = user;
        log.info("Created CurrentUser instance for username: {}", user.getUsername());
    }
}