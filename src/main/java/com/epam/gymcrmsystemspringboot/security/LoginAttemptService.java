package com.epam.gymcrmsystemspringboot.security;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * @author Arpi Khachatryan
 * @date 25.06.2024
 */

@Slf4j
@Service
public class LoginAttemptService {

    private static final int MAX_ATTEMPT = 3;
    private static final int BLOCK_DURATION_MINUTES = 5;
    private final LoadingCache<String, Integer> attemptsCache;
    private final LoadingCache<String, Long> blockedCache;

    public LoginAttemptService() {
        super();
        attemptsCache = CacheBuilder.newBuilder().expireAfterWrite(BLOCK_DURATION_MINUTES, TimeUnit.MINUTES).build(new CacheLoader<String, Integer>() {
            @Override
            public Integer load(final String key) {
                return 0;
            }
        });

        blockedCache = CacheBuilder.newBuilder().expireAfterWrite(BLOCK_DURATION_MINUTES, TimeUnit.MINUTES).build(new CacheLoader<String, Long>() {
            @Override
            public Long load(final String key) {
                return 0L;
            }
        });
    }

    public void loginFailed(final String key) {
        int attempts = 0;
        try {
            attempts = attemptsCache.get(key);
        } catch (final ExecutionException e) {
            log.error("Failed to get login attempts from cache for key: {}", key, e);
        }
        attempts++;
        attemptsCache.put(key, attempts);
        log.debug("Login failed for key: {}. Attempts so far: {}", key, attempts);
        if (attempts >= MAX_ATTEMPT) {
            blockedCache.put(key, System.currentTimeMillis());
        }
    }

    public boolean isBlocked(String key) {
        try {
            return blockedCache.get(key) > 0;
        } catch (final ExecutionException e) {
            log.error("Failed to check blocked status for key: {}", key, e);
            return false;
        }
    }

    public void loginSucceeded(final String key) {
        attemptsCache.invalidate(key);
        blockedCache.invalidate(key);
        log.debug("Login succeeded for key: {}", key);
    }
}