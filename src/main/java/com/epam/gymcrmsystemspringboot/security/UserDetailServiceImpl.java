package com.epam.gymcrmsystemspringboot.security;

import com.epam.gymcrmsystemspringboot.entity.Trainee;
import com.epam.gymcrmsystemspringboot.entity.Trainer;
import com.epam.gymcrmsystemspringboot.entity.User;
import com.epam.gymcrmsystemspringboot.repository.TraineeRepository;
import com.epam.gymcrmsystemspringboot.repository.TrainerRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * @author Arpi Khachatryan
 * @date 23.06.2024
 */

@Slf4j
@Service
@RequiredArgsConstructor
public class UserDetailServiceImpl implements UserDetailsService {

    private final TraineeRepository traineeRepository;
    private final TrainerRepository trainerRepository;
    private final LoginAttemptService loginAttemptService;
    private final HttpServletRequest request;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        log.info("Loading user by username: {}", username);

        String clientIP = getClientIP();
        if (loginAttemptService.isBlocked(clientIP)) {
            log.error("IP {} is blocked due to too many unsuccessful login attempts", clientIP);
            throw new RuntimeException("Blocked due to too many unsuccessful login attempts.");
        }

        Optional<Trainee> traineeOptional = traineeRepository.findByUsername(username);
        Optional<Trainer> trainerOptional = trainerRepository.findByUsername(username);

        User user = null;

        if (traineeOptional.isPresent()) {
            user = traineeOptional.get();
            log.info("Trainee found with username '{}'", username);
        } else if (trainerOptional.isPresent()) {
            user = trainerOptional.get();
            log.info("Trainer found with username '{}'", username);
        } else {
            log.error("User with username '{}' not found", username);
            throw new UsernameNotFoundException("User with username '" + username + "' does not exist");
        }
        log.info("User loaded: {}", user.getUsername());

        loginAttemptService.loginSucceeded(clientIP);

        return new CurrentUser(user);
    }


    private String getClientIP() {
        String xfHeader = request.getHeader("X-Forwarded-For");
        if (xfHeader == null || xfHeader.isEmpty()) {
            return request.getRemoteAddr();
        }
        return xfHeader.split(",")[0];
    }
}