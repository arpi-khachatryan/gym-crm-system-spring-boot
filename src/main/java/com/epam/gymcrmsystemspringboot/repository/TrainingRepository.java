package com.epam.gymcrmsystemspringboot.repository;

import com.epam.gymcrmsystemspringboot.entity.Training;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @author Arpi Khachatryan
 * @date 28.05.2024
 */

@Repository
public interface TrainingRepository extends JpaRepository<Training, Long>, JpaSpecificationExecutor<Training> {
    @Query("SELECT t FROM Training t " +
            "JOIN t.trainingType tt " +
            "WHERE t.trainee.username = :traineeUsername " +
            "AND (:trainerUsername IS NULL OR t.trainer.username = :trainerUsername) " +
            "AND (:periodFrom IS NULL OR t.trainingDate >= :periodFrom) " +
            "AND (:periodTo IS NULL OR t.trainingDate <= :periodTo) " +
            "AND (:trainingType IS NULL OR tt.name = :trainingType)")
    List<Training> findTrainingsByCriteria(@Param("traineeUsername") String traineeUsername,
                                           @Param("trainerUsername") String trainerUsername,
                                           @Param("periodFrom") Date periodFrom,
                                           @Param("periodTo") Date periodTo,
                                           @Param("trainingType") String trainingType);

    @Query("SELECT t FROM Training t WHERE t.name = :name AND t.trainingDate = :trainingDate AND t.duration = :duration AND t.trainee.username = :traineeUsername AND t.trainer.username = :trainerUsername AND t.trainingType.name = :trainingTypeName")
    Optional<Training> findByNameDateDurationAndTraineeTrainerUsernamesAndTrainingTypeName(@Param("name") String name, @Param("trainingDate") Date trainingDate, @Param("duration") int duration, @Param("traineeUsername") String traineeUsername, @Param("trainerUsername") String trainerUsername, @Param("trainingTypeName") String trainingTypeName);
}