package com.epam.gymcrmsystemspringboot.repository;

import com.epam.gymcrmsystemspringboot.entity.Trainer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author Arpi Khachatryan
 * @date 28.05.2024
 */

@Repository
public interface TrainerRepository extends JpaRepository<Trainer, Long> {
    boolean existsByUsername(String username);

    @Query("SELECT t FROM Trainer t WHERE t.username = :username")
    Optional<Trainer> findByUsername(@Param("username") String username);

    @Query("SELECT t FROM Trainer t WHERE t.isActive = true AND t.id NOT IN (SELECT tr.id FROM Trainer tr JOIN tr.trainees ttr WHERE ttr.username = :traineeUsername)")
    List<Trainer> findUnassignedTrainersByTraineeUsername(@Param("traineeUsername") String traineeUsername);

    List<Trainer> findAllByUsernameIn(List<String> username);
}