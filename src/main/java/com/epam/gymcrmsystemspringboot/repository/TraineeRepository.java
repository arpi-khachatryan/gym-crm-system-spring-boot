package com.epam.gymcrmsystemspringboot.repository;

import com.epam.gymcrmsystemspringboot.entity.Trainee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author Arpi Khachatryan
 * @date 28.05.2024
 */

@Repository
public interface TraineeRepository extends JpaRepository<Trainee, Long> {
    @Query("SELECT t FROM Trainee t LEFT JOIN FETCH t.trainings WHERE t.username = :username")
    Optional<Trainee> findByUsernameWithTrainings(@Param("username") String username);

    @Query("SELECT t FROM Trainee t WHERE t.username = :username")
    Optional<Trainee> findByUsername(@Param("username") String username);

    boolean existsByUsername(String username);

    @Query("SELECT DISTINCT t FROM Trainee t LEFT JOIN FETCH t.trainers WHERE t.username = :username")
    Optional<Trainee> findByUsernameWithTrainers(@Param("username") String username);
}


