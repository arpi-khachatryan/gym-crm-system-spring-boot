package com.epam.gymcrmsystemspringboot.repository;

import com.epam.gymcrmsystemspringboot.entity.TrainingType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Arpi Khachatryan
 * @date 28.05.2024
 */

@Repository
public interface TrainingTypeRepository extends JpaRepository<TrainingType, Long> {
    TrainingType findByName(String trainingTypeName);

    boolean existsByName(String name);
}