package com.epam.gymcrmsystemspringboot.dto.trainee;

import com.epam.gymcrmsystemspringboot.dto.trainer.TrainerDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * @author Arpi Khachatryan
 * @date 28.05.2024
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TraineeUpdateResponseDTO {
    private String username;
    private String firstName;
    private String lastName;
    private Date dateOfBirth;
    private String address;
    private boolean isActive;
    private List<TrainerDTO> trainers;
}
