package com.epam.gymcrmsystemspringboot.dto.trainee;

import com.epam.gymcrmsystemspringboot.dto.trainer.TrainerUpdateDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * @author Arpi Khachatryan
 * @date 28.05.2024
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TraineeTrainersUpdateRequestDTO {
    @NotBlank
    private String username;
    @NotNull
    @Size(min = 1)
    private List<TrainerUpdateDTO> trainerUpdateDTOList;
}