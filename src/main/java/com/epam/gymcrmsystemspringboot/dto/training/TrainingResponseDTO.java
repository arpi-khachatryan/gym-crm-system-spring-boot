package com.epam.gymcrmsystemspringboot.dto.training;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author Arpi Khachatryan
 * @date 28.05.2024
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TrainingResponseDTO {
    private Long id;
    private String name;
    private Date trainingDate;
    private int duration;
}