package com.epam.gymcrmsystemspringboot.dto.training;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author Arpi Khachatryan
 * @date 28.05.2024
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TraineeTrainingSearchCriteriaDTO {
    @NotNull
    private Date fromDate;
    @NotNull
    private Date toDate;
    @NotBlank
    private String trainerName;
    @NotBlank
    private String trainingTypeName;
}