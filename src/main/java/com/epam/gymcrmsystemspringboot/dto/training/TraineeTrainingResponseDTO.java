package com.epam.gymcrmsystemspringboot.dto.training;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author Arpi Khachatryan
 * @date 28.05.2024
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TraineeTrainingResponseDTO {
    private String trainingName;
    private Date trainingDate;
    private String trainingTypeName;
    private int trainingDuration;
    private String trainerName;
}