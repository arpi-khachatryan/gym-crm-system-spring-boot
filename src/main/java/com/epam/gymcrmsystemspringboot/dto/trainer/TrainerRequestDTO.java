package com.epam.gymcrmsystemspringboot.dto.trainer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.Set;

/**
 * @author Arpi Khachatryan
 * @date 28.05.2024
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TrainerRequestDTO {
    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;
    @NotBlank
    private String trainingTypeName;
    private Set<Long> traineeIds;
}