package com.epam.gymcrmsystemspringboot.dto.trainer;

import com.epam.gymcrmsystemspringboot.dto.trainee.TraineeDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Arpi Khachatryan
 * @date 28.05.2024
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TrainerUpdateResponseDTO {
    private String username;
    private String firstName;
    private String lastName;
    private String specialization;
    private boolean isActive;
    private List<TraineeDTO> trainees;
}
