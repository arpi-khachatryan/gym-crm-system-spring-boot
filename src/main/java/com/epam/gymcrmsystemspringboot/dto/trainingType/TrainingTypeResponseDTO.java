package com.epam.gymcrmsystemspringboot.dto.trainingType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Arpi Khachatryan
 * @date 28.05.2024
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TrainingTypeResponseDTO {
    private Long trainingTypeId;
    private String trainingTypeName;
}
