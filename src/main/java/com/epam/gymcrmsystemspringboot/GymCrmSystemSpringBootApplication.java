package com.epam.gymcrmsystemspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GymCrmSystemSpringBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(GymCrmSystemSpringBootApplication.class, args);
    }

}