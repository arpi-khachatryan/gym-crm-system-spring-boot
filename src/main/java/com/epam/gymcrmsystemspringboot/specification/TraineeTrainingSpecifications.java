package com.epam.gymcrmsystemspringboot.specification;

import com.epam.gymcrmsystemspringboot.entity.Training;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import java.util.Date;

/**
 * @author Arpi Khachatryan
 * @date 30.05.2024
 */

public class TraineeTrainingSpecifications {

    /**
     * Constructs a Specification to filter trainings based on the provided trainer's username.
     *
     * @param trainerUsername The username of the trainer to filter trainings.
     * @return A Specification to filter trainings based on the trainer's username.
     */
    public static Specification<Training> hasTrainerUsername(String trainerUsername) {
        return (root, query, cb) -> {
            if (trainerUsername == null || trainerUsername.isEmpty()) return cb.conjunction();
            Join<Object, Object> trainer = root.join("trainer", JoinType.INNER);
            return cb.equal(trainer.get("username"), trainerUsername);
        };
    }

    /**
     * Constructs a Specification to filter trainings based on the provided trainee's username.
     *
     * @param traineeUsername The username of the trainee to filter trainings.
     * @return A Specification to filter trainings based on the trainee's username.
     */
    public static Specification<Training> hasTraineeUsername(String traineeUsername) {
        return (root, query, cb) -> {
            Join<Object, Object> trainee = root.join("trainee", JoinType.INNER);
            return cb.equal(trainee.get("username"), traineeUsername);
        };
    }

    /**
     * Constructs a Specification to filter trainings based on the provided training type name.
     *
     * @param trainingType The name of the training type to filter trainings.
     * @return A Specification to filter trainings based on the training type name.
     */
    public static Specification<Training> hasTrainingTypeName(String trainingType) {
        return (root, query, cb) -> {
            if (trainingType == null || trainingType.isEmpty()) return cb.conjunction();
            Join<Object, Object> trainingTypeJoin = root.join("trainingType", JoinType.INNER);
            return cb.equal(trainingTypeJoin.get("name"), trainingType);
        };
    }

    /**
     * Constructs a Specification to filter trainings based on the provided date range.
     *
     * @param periodFrom The start date of the period to filter trainings.
     * @param periodTo   The end date of the period to filter trainings.
     * @return A Specification to filter trainings based on the provided date range.
     */
    public static Specification<Training> trainingDateBetween(Date periodFrom, Date periodTo) {
        return (root, query, cb) -> {
            if (periodFrom == null && periodTo == null) return cb.conjunction();
            if (periodFrom == null) return cb.lessThanOrEqualTo(root.get("trainingDate"), periodTo);
            if (periodTo == null) return cb.greaterThanOrEqualTo(root.get("trainingDate"), periodFrom);
            return cb.between(root.get("trainingDate"), periodFrom, periodTo);
        };
    }
}