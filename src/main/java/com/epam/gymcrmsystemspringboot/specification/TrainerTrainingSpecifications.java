package com.epam.gymcrmsystemspringboot.specification;

import com.epam.gymcrmsystemspringboot.entity.Training;
import org.springframework.data.jpa.domain.Specification;

import java.util.Date;

/**
 * @author Arpi Khachatryan
 * @date 30.05.2024
 */

public class TrainerTrainingSpecifications {

    /**
     * Constructs a Specification to filter trainings based on the provided trainee's name.
     *
     * @param traineeName The name of the trainee to filter trainings.
     * @return A Specification to filter trainings based on the trainee's name.
     */
    public static Specification<Training> hasTraineeName(String traineeName) {
        return (root, query, cb) -> cb.equal(root.get("trainee").get("username"), traineeName);
    }

    /**
     * Constructs a Specification to filter trainings based on the provided date range.
     *
     * @param fromDate The start date of the period to filter trainings.
     * @param toDate   The end date of the period to filter trainings.
     * @return A Specification to filter trainings based on the provided date range.
     */
    public static Specification<Training> trainingDateBetween(Date fromDate, Date toDate) {
        return (root, query, cb) -> cb.between(root.get("trainingDate"), fromDate, toDate);
    }

    /**
     * Constructs a Specification to filter trainings based on the provided trainer's username.
     *
     * @param trainerUsername The username of the trainer to filter trainings.
     * @return A Specification to filter trainings based on the trainer's username.
     */
    public static Specification<Training> hasTrainerUsername(String trainerUsername) {
        return (root, query, cb) -> cb.equal(root.get("trainer").get("username"), trainerUsername);
    }
}