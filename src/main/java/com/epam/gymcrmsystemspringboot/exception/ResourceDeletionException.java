package com.epam.gymcrmsystemspringboot.exception;

/**
 * @author Arpi Khachatryan
 * @date 10.06.2024
 */

public class ResourceDeletionException extends RuntimeException {
    public ResourceDeletionException(String message) {
        super(message);
    }

    public ResourceDeletionException(String message, Throwable cause) {
        super(message, cause);
    }
}
