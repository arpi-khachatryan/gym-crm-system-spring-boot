package com.epam.gymcrmsystemspringboot.exception;

/**
 * @author Arpi Khachatryan
 * @date 10.06.2024
 */

public class ResourceNotFoundException extends RuntimeException {
    public ResourceNotFoundException(String message) {
        super(message);
    }

    public ResourceNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
