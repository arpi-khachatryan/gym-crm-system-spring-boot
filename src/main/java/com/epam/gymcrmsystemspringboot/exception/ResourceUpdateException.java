package com.epam.gymcrmsystemspringboot.exception;

/**
 * @author Arpi Khachatryan
 * @date 10.06.2024
 */

public class ResourceUpdateException extends RuntimeException {
    public ResourceUpdateException(String message) {
        super(message);
    }

    public ResourceUpdateException(String message, Throwable cause) {
        super(message, cause);
    }
}
