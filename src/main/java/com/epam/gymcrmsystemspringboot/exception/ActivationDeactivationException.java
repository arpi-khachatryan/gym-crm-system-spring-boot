package com.epam.gymcrmsystemspringboot.exception;

/**
 * @author Arpi Khachatryan
 * @date 10.06.2024
 */

public class ActivationDeactivationException extends RuntimeException {
    public ActivationDeactivationException(String message) {
        super(message);
    }

    public ActivationDeactivationException(String message, Throwable cause) {
        super(message, cause);
    }
}
