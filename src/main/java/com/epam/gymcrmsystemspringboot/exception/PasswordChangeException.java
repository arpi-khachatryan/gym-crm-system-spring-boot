package com.epam.gymcrmsystemspringboot.exception;

/**
 * @author Arpi Khachatryan
 * @date 10.06.2024
 */

public class PasswordChangeException extends RuntimeException {
    public PasswordChangeException(String message) {
        super(message);
    }

    public PasswordChangeException(String message, Throwable cause) {
        super(message, cause);
    }
}
