package com.epam.gymcrmsystemspringboot.exception;

/**
 * @author Arpi Khachatryan
 * @date 10.06.2024
 */

public class ResourceCreationException extends RuntimeException {
    public ResourceCreationException(String message) {
        super(message);
    }

    public ResourceCreationException(String message, Throwable cause) {
        super(message, cause);
    }
}

