package com.epam.gymcrmsystemspringboot.actuator;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

/**
 * @author Arpi Khachatryan
 * @date 21.06.2024
 */

@Slf4j
@Component
public class MemoryHealthIndicator implements HealthIndicator {

    private final long threshold;

    public MemoryHealthIndicator(@Value("${memory.health.threshold}") long threshold) {
        this.threshold = threshold;
    }

    @Override
    public Health health() {
        long freeMemory = Runtime.getRuntime().freeMemory();
        log.info("Checking memory health. Free memory: {} bytes", freeMemory);

        if (freeMemory >= threshold) {
            log.info("Memory health is UP. Free memory is above the threshold of {} bytes", threshold);
            return Health.up().withDetail("freeMemory", freeMemory).build();
        } else {
            log.warn("Memory health is DOWN. Free memory is below the threshold of {} bytes", threshold);
            return Health.down().withDetail("freeMemory", freeMemory).build();
        }
    }
}