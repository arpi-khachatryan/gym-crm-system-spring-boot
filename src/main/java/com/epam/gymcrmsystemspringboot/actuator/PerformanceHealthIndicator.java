package com.epam.gymcrmsystemspringboot.actuator;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;

/**
 * @author Arpi Khachatryan
 * @date 21.06.2024
 */

@Slf4j
@Component
public class PerformanceHealthIndicator implements HealthIndicator {

    private final double cpuLoadThreshold;

    public PerformanceHealthIndicator(@Value("${performance.health.cpuLoadThreshold}") double cpuLoadThreshold) {
        this.cpuLoadThreshold = cpuLoadThreshold;
    }

    @Override
    public Health health() {
        OperatingSystemMXBean osBean = ManagementFactory.getOperatingSystemMXBean();
        double systemCpuLoad = osBean.getSystemLoadAverage();

        log.info("Checking performance health. System CPU load: {}", systemCpuLoad);

        if (systemCpuLoad >= 0 && systemCpuLoad < cpuLoadThreshold) {
            log.info("Performance health is UP. System CPU load is below the threshold of {}", cpuLoadThreshold);
            return Health.up().withDetail("systemCpuLoad", systemCpuLoad).build();
        } else {
            log.warn("Performance health is DOWN. System CPU load is above the threshold of {}", cpuLoadThreshold);
            return Health.down().withDetail("systemCpuLoad", systemCpuLoad).build();
        }
    }
}