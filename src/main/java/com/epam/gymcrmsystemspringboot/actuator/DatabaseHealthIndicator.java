package com.epam.gymcrmsystemspringboot.actuator;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author Arpi Khachatryan
 * @date 21.06.2024
 */

@Slf4j
@Component
public class DatabaseHealthIndicator implements HealthIndicator {

    private final DataSource dataSource;
    private final int timeout;

    @Autowired
    public DatabaseHealthIndicator(DataSource dataSource, @Value("${database.health.timeout}") int timeout) {
        this.dataSource = dataSource;
        this.timeout = timeout;
    }

    @Override
    public Health health() {
        try (Connection connection = dataSource.getConnection()) {
            if (connection.isValid(timeout)) {
                return Health.up().build();
            } else {
                log.error("Database connection is not valid");
                return Health.down().withDetail("Error", "Database connection is not valid").build();
            }
        } catch (SQLException e) {
            log.error("Database connection check failed", e);
            return Health.down(e).build();
        }
    }
}