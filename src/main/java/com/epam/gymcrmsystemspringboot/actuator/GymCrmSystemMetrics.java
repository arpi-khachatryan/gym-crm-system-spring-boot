package com.epam.gymcrmsystemspringboot.actuator;

import io.micrometer.core.instrument.DistributionSummary;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Arpi Khachatryan
 * @date 20.06.2024
 */

@Slf4j
@Component
public class GymCrmSystemMetrics {

    private final MeterRegistry meterRegistry;
    private final AtomicInteger activeSessions = new AtomicInteger(0);
    private final DistributionSummary responseSizes;

    @Autowired
    public GymCrmSystemMetrics(MeterRegistry meterRegistry) {
        this.meterRegistry = meterRegistry;
        this.responseSizes = DistributionSummary.builder("response.sizes")
                .register(meterRegistry);
    }

    public void incrementTraineeCount() {
        meterRegistry.counter("trainee.created.count").increment();
        log.debug("Incremented trainee count");
    }

    public void incrementTrainerCount() {
        meterRegistry.counter("trainer.created.count").increment();
        log.debug("Incremented trainer count");
    }

    public void incrementTrainingCount() {
        meterRegistry.counter("training.created.count").increment();
        log.debug("Incremented training count");
    }

    public void incrementTrainingTypeCount() {
        meterRegistry.counter("trainingType.fetched.count").increment();
        log.debug("Incremented training type count");
    }

    public Timer.Sample startTimer(String timerName) {
        log.debug("Started timer: {}", timerName);
        return Timer.start(meterRegistry);
    }

    public void stopTimer(Timer.Sample sample, String timerName) {
        sample.stop(meterRegistry.timer(timerName));
        log.debug("Stopped timer: {}", timerName);
    }

    public void incrementActiveSessions() {
        activeSessions.incrementAndGet();
        meterRegistry.gauge("active.sessions", activeSessions);
        log.debug("Incremented active sessions: {}", activeSessions.get());
    }

    public void decrementActiveSessions() {
        activeSessions.decrementAndGet();
        meterRegistry.gauge("active.sessions", activeSessions);
        log.debug("Decremented active sessions: {}", activeSessions.get());
    }

    public void recordResponseSize(double size) {
        responseSizes.record(size);
        log.debug("Recorded response size: {}", size);
    }
}