package com.epam.gymcrmsystemspringboot.util;

import java.util.UUID;

/**
 * @author Arpi Khachatryan
 * @date 09.06.2024
 */

public class LoggingUtils {
    public static String generateTransactionId() {
        return UUID.randomUUID().toString();
    }
}
