package com.epam.gymcrmsystemspringboot.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Arpi Khachatryan
 * @date 18.05.2024
 */

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "trainers")
public class Trainer extends User {
    @ManyToOne
    @JoinColumn(name = "training_type_id", nullable = false)
    private TrainingType trainingType;
    @ManyToMany
    @JoinTable(name = "trainer_trainee",
            joinColumns = @JoinColumn(name = "trainer_id"),
            inverseJoinColumns = @JoinColumn(name = "trainee_id"))
    private List<Trainee> trainees = new ArrayList<>();

    public Trainer(Long id, String firstName, String lastName, String username, String password, boolean isActive, TrainingType trainingType, List<Trainee> trainees) {
        super(id, firstName, lastName, username, password, isActive);
        this.trainingType = trainingType;
        this.trainees = trainees;
    }
}