package com.epam.gymcrmsystemspringboot.parametrs;

import com.epam.gymcrmsystemspringboot.dto.general.LoginRequestDTO;
import com.epam.gymcrmsystemspringboot.dto.general.PasswordChangeDTO;
import com.epam.gymcrmsystemspringboot.dto.general.UserActivationDTO;
import com.epam.gymcrmsystemspringboot.dto.trainee.*;
import com.epam.gymcrmsystemspringboot.dto.trainer.*;
import com.epam.gymcrmsystemspringboot.dto.training.TraineeTrainingResponseDTO;
import com.epam.gymcrmsystemspringboot.dto.training.TrainerTrainingResponseDTO;
import com.epam.gymcrmsystemspringboot.dto.training.TrainingRequestDTO;
import com.epam.gymcrmsystemspringboot.dto.trainingType.TrainingTypeResponseDTO;
import com.epam.gymcrmsystemspringboot.entity.Trainee;
import com.epam.gymcrmsystemspringboot.entity.Trainer;
import com.epam.gymcrmsystemspringboot.entity.Training;
import com.epam.gymcrmsystemspringboot.entity.TrainingType;

import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author Arpi Khachatryan
 * @date 21.05.2024
 */

public class MockData {

    public static Trainee getTrainee() {
        Trainee trainee = new Trainee();
        trainee.setUsername("firstName.lastName");
        trainee.setFirstName("firstName");
        trainee.setLastName("lastName");
        trainee.setDateOfBirth(new Date());
        trainee.setAddress("address");
        trainee.setActive(true);
        trainee.setTrainers(Collections.emptyList());
        return trainee;
    }

    public static TraineeResponseDTO getTraineeResponseDTO() {
        return TraineeResponseDTO.builder()
                .username("firstName.lastName")
                .password("password1234")
                .build();
    }

    public static TraineeGetResponseDTO getTraineeGetResponseDTO() {
        return TraineeGetResponseDTO.builder()
                .firstName("firstName")
                .lastName("lastName")
                .address("address")
                .dateOfBirth(new Date(2024, 05, 21))
                .isActive(true)
                .build();
    }

    public static TraineeRequestDTO getTraineeRequestDTO() {
        return TraineeRequestDTO.builder()
                .firstName("firstName")
                .lastName("lastName")
                .address("address")
                .dateOfBirth(new Date(2024, 05, 21))
                .build();
    }

    public static TraineeUpdateRequestDTO getTraineeUpdateRequestDTO() {
        return TraineeUpdateRequestDTO.builder()
                .username("firstName.lastName")
                .firstName("firstName")
                .lastName("lastName")
                .address("address_2")
                .dateOfBirth(new Date(2024, 05, 21))
                .isActive(true)
                .build();
    }

    public static TraineeUpdateResponseDTO getTraineeUpdateResponseDTO() {
        return TraineeUpdateResponseDTO.builder()
                .username(getTrainee().getUsername())
                .firstName(getTrainee().getFirstName())
                .lastName(getTrainee().getLastName())
                .dateOfBirth(getTrainee().getDateOfBirth())
                .address(getTrainee().getAddress())
                .isActive(getTrainee().isActive())
                .trainers(null)
                .build();
    }

    public static Trainee getTrainee1Data(String username) {
        Trainee trainee = new Trainee();
        trainee.setUsername(username);
        return trainee;
    }

    public static TrainingType getTrainingType() {
        return TrainingType.builder()
                .id(1L)
                .name("Cardio")
                .build();
    }

    public static TrainingTypeResponseDTO getTrainingTypeResponseDTO() {
        return TrainingTypeResponseDTO.builder()
                .trainingTypeId(1L)
                .trainingTypeName("Fitness")
                .build();
    }

    public static Trainer getTrainer() {
        return new Trainer(
                1L,
                "firstName",
                "lastName",
                "firstName.lastName",
                "password1234",
                true,
                new TrainingType(1L, "Cardio"),
                List.of(getTrainee())
        );
    }

    public static TrainerRequestDTO getTrainerRequestDTO() {
        return TrainerRequestDTO.builder()
                .firstName("firstName")
                .lastName("lastName")
                .trainingTypeName(String.valueOf(new TrainingType(1L, "Cardio")))
                .build();
    }

    public static TrainerResponseDTO getTrainerResponseDTO() {
        return TrainerResponseDTO.builder()
                .username("firstName.lastName")
                .password("password1234")
                .build();
    }

    public static TrainerUpdateRequestDTO getTrainerUpdateRequestDTO() {
        return TrainerUpdateRequestDTO.builder()
                .username("firstName.lastName")
                .firstName("firstName")
                .lastName("lastName")
                .specialization("Cardio")
                .isActive(true)
                .build();
    }

    public static TrainerGetResponseDTO getTrainerGetResponseDTO() {
        return TrainerGetResponseDTO.builder()
                .firstName("firstName")
                .lastName("lastName")
                .specialization("Cardio")
                .isActive(true)
                .build();
    }

    public static TrainerUpdateResponseDTO getTrainerUpdateResponseDTO() {
        return TrainerUpdateResponseDTO.builder()
                .username(getTrainer().getUsername())
                .firstName(getTrainer().getFirstName())
                .lastName(getTrainer().getLastName())
                .specialization(String.valueOf(getTrainer().getTrainingType()))
                .isActive(getTrainee().isActive())
                .trainees(null)
                .build();
    }

    public static TrainerDTO getTrainerDTOData(String username) {
        return TrainerDTO.builder()
                .username(username)
                .firstName("FirstName")
                .lastName("LastName")
                .specialization("Specialization")
                .build();
    }

    public static Trainer getTrainerData(String username, String firstName, String lastName, String specialization) {
        Trainer trainer = new Trainer();
        trainer.setUsername(username);
        trainer.setFirstName(firstName);
        trainer.setLastName(lastName);
        trainer.setTrainingType(getTrainingType());
        return trainer;
    }

    public static TrainerDTO getTrainerDTOData(String username, String firstName, String lastName, String specialization) {
        return TrainerDTO.builder()
                .username(username)
                .firstName(firstName)
                .lastName(lastName)
                .specialization(specialization)
                .build();
    }

    public static Trainer getTrainerData(String username) {
        Trainer trainer = new Trainer();
        trainer.setUsername(username);
        return trainer;
    }

    public static Training getTraining() {
        return Training
                .builder()
                .name("name")
                .duration(60)
                .trainee(getTrainee())
                .trainer(getTrainer())
                .trainingType(getTrainingType())
                .build();
    }

    public static TrainingRequestDTO getTrainingRequestDTO() {
        return TrainingRequestDTO.builder()
                .trainingName("Morning Workout")
                .trainingDate(new Date(2024, 05, 21))
                .trainingDuration(12)
                .traineeUsername(getTrainee().getUsername())
                .trainerUsername(getTrainer().getUsername())
                .trainingTypeName(getTrainingType().getName())
                .build();
    }

    public static Training getTrainingData(String trainingName, Date trainingDate, int trainingDuration) {
        Training training = new Training();
        training.setName(trainingName);
        training.setTrainer(new Trainer());
        training.setTrainee(new Trainee());
        training.setTrainingType(getTrainingType());
        training.setTrainingDate(trainingDate);
        training.setDuration(trainingDuration);
        return training;
    }

    public static TraineeTrainingResponseDTO getTrainingDTOData(String trainingName, String trainerName, String trainingTypeName, Date trainingDate, int trainingDuration) {
        return TraineeTrainingResponseDTO.builder()
                .trainingName(trainingName)
                .trainingDate(trainingDate)
                .trainingTypeName(trainingTypeName)
                .trainingDuration(trainingDuration)
                .trainerName(trainerName)
                .build();
    }

    public static TrainerTrainingResponseDTO getTrainerTrainingDTOData(String trainingName, String traineeName, String trainingTypeName, Date trainingDate, int trainingDuration) {
        return TrainerTrainingResponseDTO.builder()
                .trainingName(trainingName)
                .trainingDate(trainingDate)
                .trainingTypeName(trainingTypeName)
                .trainingDuration(trainingDuration)
                .traineeName(traineeName)
                .build();
    }

    public static UserActivationDTO getUserActivationDTO() {
        return UserActivationDTO.builder()
                .username("firstName.lastName")
                .isActive(false)
                .build();
    }

    public static PasswordChangeDTO getPasswordChangeDTO() {
        return PasswordChangeDTO.builder()
                .userId(1L)
                .oldPassword("oldPassword")
                .newPassword("newPassword")
                .build();
    }

    public static LoginRequestDTO getLoginResponseDTO() {
        return LoginRequestDTO.builder()
                .username(getTrainee().getUsername())
                .password(getTrainee().getPassword())
                .build();
    }
}