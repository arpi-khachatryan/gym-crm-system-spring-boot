package com.epam.gymcrmsystemspringboot.seriveImpl;

import com.epam.gymcrmsystemspringboot.dto.training.TrainingRequestDTO;
import com.epam.gymcrmsystemspringboot.entity.Trainee;
import com.epam.gymcrmsystemspringboot.entity.Trainer;
import com.epam.gymcrmsystemspringboot.entity.Training;
import com.epam.gymcrmsystemspringboot.entity.TrainingType;
import com.epam.gymcrmsystemspringboot.exception.ResourceCreationException;
import com.epam.gymcrmsystemspringboot.repository.TraineeRepository;
import com.epam.gymcrmsystemspringboot.repository.TrainerRepository;
import com.epam.gymcrmsystemspringboot.repository.TrainingRepository;
import com.epam.gymcrmsystemspringboot.repository.TrainingTypeRepository;
import com.epam.gymcrmsystemspringboot.service.serviceImpl.TrainingServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;
import java.util.Optional;

import static com.epam.gymcrmsystemspringboot.parametrs.MockData.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

/**
 * @author Arpi Khachatryan
 * @date 21.05.2024
 */

@ExtendWith(MockitoExtension.class)
public class TrainingServiceImplTest {

    @InjectMocks
    private TrainingServiceImpl trainingService;

    @Mock
    private TrainingRepository trainingRepository;

    @Mock
    private TraineeRepository traineeRepository;

    @Mock
    private TrainerRepository trainerRepository;

    @Mock
    private TrainingTypeRepository trainingTypeRepository;

    @Test
    public void testCreateTraining() {
        TrainingRequestDTO trainingRequestDTO = getTrainingRequestDTO();
        Trainee trainee = getTrainee();
        Trainer trainer = getTrainer();
        TrainingType trainingType = getTrainingType();

        when(traineeRepository.findByUsername(anyString())).thenReturn(Optional.of(trainee));
        when(trainerRepository.findByUsername(anyString())).thenReturn(Optional.of(trainer));
        when(trainingTypeRepository.findByName(anyString())).thenReturn(trainingType);
        when(trainingRepository.findByNameDateDurationAndTraineeTrainerUsernamesAndTrainingTypeName(anyString(), any(Date.class), anyInt(), anyString(), anyString(), anyString())).thenReturn(Optional.empty());
        when(trainingRepository.save(any(Training.class))).thenReturn(new Training());
        trainingService.createTraining(trainingRequestDTO, "transactionId");

        verify(traineeRepository, times(1)).findByUsername(trainingRequestDTO.getTraineeUsername());
        verify(trainerRepository, times(1)).findByUsername(trainingRequestDTO.getTrainerUsername());
        verify(trainingTypeRepository, times(1)).findByName(trainingRequestDTO.getTrainingTypeName());
        verify(trainingRepository, times(1)).findByNameDateDurationAndTraineeTrainerUsernamesAndTrainingTypeName(trainingRequestDTO.getTrainingName(), trainingRequestDTO.getTrainingDate(), trainingRequestDTO.getTrainingDuration(), trainingRequestDTO.getTraineeUsername(), trainingRequestDTO.getTrainerUsername(), trainingRequestDTO.getTrainingTypeName());
        verify(trainingRepository, times(1)).save(any(Training.class));
    }

    @Test
    public void testCreateTraining_TrainingAlreadyExists() {
        TrainingRequestDTO trainingRequestDTO = getTrainingRequestDTO();
        Trainee trainee = getTrainee();
        Trainer trainer = getTrainer();
        TrainingType trainingType = getTrainingType();
        Training existingTraining = getTraining();

        when(traineeRepository.findByUsername(anyString())).thenReturn(Optional.of(trainee));
        when(trainerRepository.findByUsername(anyString())).thenReturn(Optional.of(trainer));
        when(trainingTypeRepository.findByName(anyString())).thenReturn(trainingType);
        when(trainingRepository.findByNameDateDurationAndTraineeTrainerUsernamesAndTrainingTypeName(anyString(), any(Date.class), anyInt(), anyString(), anyString(), anyString())).thenReturn(Optional.of(existingTraining));
        assertThrows(ResourceCreationException.class, () -> trainingService.createTraining(trainingRequestDTO, "transactionId"));

        verify(traineeRepository, times(1)).findByUsername(trainingRequestDTO.getTraineeUsername());
        verify(trainerRepository, times(1)).findByUsername(trainingRequestDTO.getTrainerUsername());
        verify(trainingTypeRepository, times(1)).findByName(trainingRequestDTO.getTrainingTypeName());
        verify(trainingRepository, times(1)).findByNameDateDurationAndTraineeTrainerUsernamesAndTrainingTypeName(trainingRequestDTO.getTrainingName(), trainingRequestDTO.getTrainingDate(), trainingRequestDTO.getTrainingDuration(), trainingRequestDTO.getTraineeUsername(), trainingRequestDTO.getTrainerUsername(), trainingRequestDTO.getTrainingTypeName());
    }
}