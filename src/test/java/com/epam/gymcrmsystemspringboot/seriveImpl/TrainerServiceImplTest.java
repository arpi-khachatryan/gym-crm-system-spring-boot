package com.epam.gymcrmsystemspringboot.seriveImpl;

import com.epam.gymcrmsystemspringboot.dto.general.UserActivationDTO;
import com.epam.gymcrmsystemspringboot.dto.trainer.*;
import com.epam.gymcrmsystemspringboot.dto.training.TrainerTrainingResponseDTO;
import com.epam.gymcrmsystemspringboot.entity.Trainer;
import com.epam.gymcrmsystemspringboot.entity.Training;
import com.epam.gymcrmsystemspringboot.mapper.TrainerMapper;
import com.epam.gymcrmsystemspringboot.mapper.TrainingMapper;
import com.epam.gymcrmsystemspringboot.repository.TrainerRepository;
import com.epam.gymcrmsystemspringboot.repository.TrainingRepository;
import com.epam.gymcrmsystemspringboot.repository.TrainingTypeRepository;
import com.epam.gymcrmsystemspringboot.service.GeneratorService;
import com.epam.gymcrmsystemspringboot.service.serviceImpl.TrainerServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static com.epam.gymcrmsystemspringboot.parametrs.MockData.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * @author Arpi Khachatryan
 * @date 21.05.2024
 */

@ExtendWith(MockitoExtension.class)
public class TrainerServiceImplTest {

    @InjectMocks
    private TrainerServiceImpl trainerService;

    @Mock
    private TrainerRepository trainerRepository;

    @Mock
    private GeneratorService generatorService;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private TrainingTypeRepository trainingTypeRepository;

    @Mock
    private TrainingRepository trainingRepository;

    @Mock
    private TrainerMapper trainerMapper;

    @Mock
    private TrainingMapper trainingMapper;

    @Test
    public void testCreateTrainer() {
        TrainerRequestDTO trainerRequestDTO = getTrainerRequestDTO();
        Trainer trainer = new Trainer();
        trainer.setFirstName(getTraineeUpdateRequestDTO().getFirstName());
        trainer.setLastName(getTraineeUpdateRequestDTO().getLastName());
        trainer.setActive(true);

        when(generatorService.calculateBaseUserName(anyString(), anyString())).thenReturn("firstName.lastName");
        when(generatorService.generateUniqueUserName(anyString())).thenReturn("firstName.lastName");
        when(generatorService.generateRandomPassword()).thenReturn("password1234");
        when(passwordEncoder.encode(anyString())).thenReturn("hashedPassword1234");
        when(trainerMapper.trainerToTrainerResponseDto(any(Trainer.class))).thenReturn(getTrainerResponseDTO());
        when(trainerRepository.save(any(Trainer.class))).thenReturn(trainer);
        TrainerResponseDTO createdTrainerResponse = trainerService.createTrainer(trainerRequestDTO, "transactionId");

        assertEquals("firstName.lastName", createdTrainerResponse.getUsername());
        assertEquals("password1234", createdTrainerResponse.getPassword());
    }

    @Test
    public void testChangePassword() {
        Trainer trainer = getTrainer();
        trainer.setPassword("oldPassword");

        when(trainerRepository.findById(1L)).thenReturn(Optional.of(trainer));
        when(trainerRepository.save(any(Trainer.class))).thenReturn(trainer);
        trainerService.changePassword(getPasswordChangeDTO(), "transactionId");

        assertEquals("newPassword", trainer.getPassword());
    }

    @Test
    public void testUpdateTrainer() {
        TrainerUpdateRequestDTO updateRequestDTO = getTrainerUpdateRequestDTO();
        Trainer existingTrainer = getTrainer();
        existingTrainer.setFirstName(updateRequestDTO.getFirstName());
        TrainerUpdateResponseDTO expectedResponseDTO = getTrainerUpdateResponseDTO();

        when(trainerRepository.findByUsername(updateRequestDTO.getUsername())).thenReturn(Optional.of(existingTrainer));
        when(trainerRepository.save(any(Trainer.class))).thenReturn(existingTrainer);
        when(trainerMapper.trainerToTrainerUpdateResponseDTO(any(Trainer.class))).thenReturn(expectedResponseDTO);
        TrainerUpdateResponseDTO actualResponseDTO = trainerService.updateTrainer(updateRequestDTO, "transactionId");

        assertEquals(expectedResponseDTO, actualResponseDTO);
    }

    @Test
    public void testActivateDe_ActivateTrainer() {
        UserActivationDTO userActivationDTO = getUserActivationDTO();
        Trainer trainer = new Trainer();
        trainer.setUsername(userActivationDTO.getUsername());
        trainer.setActive(true);

        when(trainerRepository.findByUsername(userActivationDTO.getUsername())).thenReturn(Optional.of(trainer));
        when(trainerRepository.save(any(Trainer.class))).thenReturn(trainer);
        trainerService.activateDeactivateTrainer(userActivationDTO, "transactionId");

        assertFalse(trainer.isActive());
        verify(trainerRepository, times(1)).save(trainer);
    }


    @Test
    public void testGetTrainer() {
        TrainerGetResponseDTO responseDTO = getTrainerGetResponseDTO();
        Trainer trainer = getTrainer();

        when(trainerRepository.findByUsername("firstName.lastName")).thenReturn(Optional.of(trainer));
        when(trainerMapper.trainerToTrainerGetResponseDTO(trainer)).thenReturn(responseDTO);
        TrainerGetResponseDTO result = trainerService.getTrainer("firstName.lastName", "transactionId");

        assertNotNull(result);
        assertTrue(result.isActive());
        verify(trainerRepository, times(1)).findByUsername("firstName.lastName");
        verify(trainerMapper, times(1)).trainerToTrainerGetResponseDTO(trainer);
    }

    @Test
    public void testGetTrainerTrainingsByCriteria() {
        Training training = getTrainingData("Training", new Date(), 60);
        List<Training> trainings = List.of(training);
        TrainerTrainingResponseDTO trainingDTO = getTrainerTrainingDTOData("Training", "traineeName", "trainingType", new Date(), 60);
        List<TrainerTrainingResponseDTO> trainingDTOs = Collections.singletonList(trainingDTO);

        when(trainingRepository.findAll(any(Specification.class))).thenReturn(trainings);
        when(trainingMapper.trainingToTrainerTrainingResponseDTO(trainings)).thenReturn(trainingDTOs);
        List<TrainerTrainingResponseDTO> result = trainerService.getTrainerTrainingsByCriteria("firstName.lastName", new Date(), new Date(), "traineeName", "transactionId");

        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals(training.getName(), result.get(0).getTrainingName());
        verify(trainingRepository, times(1)).findAll(any(Specification.class));
        verify(trainingMapper, times(1)).trainingToTrainerTrainingResponseDTO(trainings);
    }
}