package com.epam.gymcrmsystemspringboot.seriveImpl;

import com.epam.gymcrmsystemspringboot.dto.trainingType.TrainingTypeResponseDTO;
import com.epam.gymcrmsystemspringboot.entity.TrainingType;
import com.epam.gymcrmsystemspringboot.exception.ResourceNotFoundException;
import com.epam.gymcrmsystemspringboot.mapper.TrainingTypeMapper;
import com.epam.gymcrmsystemspringboot.repository.TrainingTypeRepository;
import com.epam.gymcrmsystemspringboot.service.serviceImpl.TrainingTypeServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.epam.gymcrmsystemspringboot.parametrs.MockData.getTrainingType;
import static com.epam.gymcrmsystemspringboot.parametrs.MockData.getTrainingTypeResponseDTO;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

/**
 * @author Arpi Khachatryan
 * @date 21.05.2024
 */

@ExtendWith(MockitoExtension.class)
public class TrainingTypeServiceImplTest {

    @InjectMocks
    private TrainingTypeServiceImpl trainingTypeService;

    @Mock
    private TrainingTypeRepository trainingTypeRepository;

    @Mock
    private TrainingTypeMapper trainingTypeMapper;

    @Test
    public void testGetTrainingTypes() {
        List<TrainingType> trainingTypes = List.of(getTrainingType());
        List<TrainingTypeResponseDTO> trainingTypeDTOs = List.of(getTrainingTypeResponseDTO());

        when(trainingTypeRepository.findAll()).thenReturn(trainingTypes);
        when(trainingTypeMapper.trainingTypeToTrainingTypeDTO(trainingTypes)).thenReturn(trainingTypeDTOs);
        List<TrainingTypeResponseDTO> result = trainingTypeService.getTrainingTypes("transactionId");

        verify(trainingTypeRepository, times(1)).findAll();
        verify(trainingTypeMapper, times(1)).trainingTypeToTrainingTypeDTO(trainingTypes);
        assertEquals(1, result.size());
        assertEquals("Fitness", result.get(0).getTrainingTypeName());
    }

    @Test
    public void testGetTrainingTypes_NotFound() {
        when(trainingTypeRepository.findAll()).thenReturn(new ArrayList<>());
        ResourceNotFoundException exception = assertThrows(ResourceNotFoundException.class, () -> trainingTypeService.getTrainingTypes("transactionId"));

        assertEquals("Error retrieving training types.", exception.getMessage());
        verify(trainingTypeRepository, times(1)).findAll();
        verify(trainingTypeMapper, never()).trainingTypeToTrainingTypeDTO(anyList());
    }
}