package com.epam.gymcrmsystemspringboot.seriveImpl;

import com.epam.gymcrmsystemspringboot.dto.general.LoginRequestDTO;
import com.epam.gymcrmsystemspringboot.dto.general.LoginResponseDTO;
import com.epam.gymcrmsystemspringboot.entity.Trainee;
import com.epam.gymcrmsystemspringboot.exception.AuthenticationException;
import com.epam.gymcrmsystemspringboot.repository.TraineeRepository;
import com.epam.gymcrmsystemspringboot.repository.TrainerRepository;
import com.epam.gymcrmsystemspringboot.security.LoginAttemptService;
import com.epam.gymcrmsystemspringboot.service.serviceImpl.AuthServiceImpl;
import com.epam.gymcrmsystemspringboot.util.JwtTokenUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

import static com.epam.gymcrmsystemspringboot.parametrs.MockData.getLoginResponseDTO;
import static com.epam.gymcrmsystemspringboot.parametrs.MockData.getTrainee;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.when;

/**
 * @author Arpi Khachatryan
 * @date 24.06.2024
 */

@ExtendWith(MockitoExtension.class)
public class AuthServiceImplTest {

    @InjectMocks
    private AuthServiceImpl authService;

    @Mock
    private TraineeRepository traineeRepository;

    @Mock
    private TrainerRepository trainerRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private JwtTokenUtil jwtTokenUtil;

    @Mock
    private HttpServletRequest request;

    @Mock
    private LoginAttemptService loginAttemptService;

    @Test
    public void testAuthenticate() {
        LoginRequestDTO loginResponseDTO = getLoginResponseDTO();
        Trainee trainee = getTrainee();

        when(traineeRepository.findByUsername(trainee.getUsername())).thenReturn(Optional.of(trainee));
        when(passwordEncoder.matches(trainee.getPassword(), loginResponseDTO.getPassword())).thenReturn(true);
        loginAttemptService.loginSucceeded(anyString());
        when(jwtTokenUtil.generateToken(trainee)).thenReturn("JWTToken");
        LoginResponseDTO responseDTO = authService.authenticate(loginResponseDTO, "transactionId");

        assertEquals("JWTToken", responseDTO.getToken());
    }

    @Test
    public void testAuthenticateUserNotFound() {
        when(traineeRepository.findByUsername(anyString())).thenReturn(Optional.empty());
        when(trainerRepository.findByUsername(anyString())).thenReturn(Optional.empty());
        LoginRequestDTO request = getLoginResponseDTO();

        assertThrows(AuthenticationException.class, () -> authService.authenticate(request, "transactionId"));
    }

    @Test
    public void testAuthenticateInvalidPassword() {
        Trainee trainee = getTrainee();

        when(traineeRepository.findByUsername(trainee.getUsername())).thenReturn(Optional.of(trainee));
        when(passwordEncoder.matches(getLoginResponseDTO().getPassword(), trainee.getPassword())).thenReturn(false);
        LoginRequestDTO request = getLoginResponseDTO();

        assertThrows(AuthenticationException.class, () -> authService.authenticate(request, "transactionId"));
    }
}