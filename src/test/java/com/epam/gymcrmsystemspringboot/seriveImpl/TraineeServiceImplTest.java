package com.epam.gymcrmsystemspringboot.seriveImpl;

import com.epam.gymcrmsystemspringboot.dto.general.UserActivationDTO;
import com.epam.gymcrmsystemspringboot.dto.trainee.*;
import com.epam.gymcrmsystemspringboot.dto.trainer.TrainerDTO;
import com.epam.gymcrmsystemspringboot.dto.trainer.TrainerUpdateDTO;
import com.epam.gymcrmsystemspringboot.dto.training.TraineeTrainingResponseDTO;
import com.epam.gymcrmsystemspringboot.entity.Trainee;
import com.epam.gymcrmsystemspringboot.entity.Trainer;
import com.epam.gymcrmsystemspringboot.entity.Training;
import com.epam.gymcrmsystemspringboot.mapper.TraineeMapper;
import com.epam.gymcrmsystemspringboot.mapper.TrainerMapper;
import com.epam.gymcrmsystemspringboot.mapper.TrainingMapper;
import com.epam.gymcrmsystemspringboot.repository.TraineeRepository;
import com.epam.gymcrmsystemspringboot.repository.TrainerRepository;
import com.epam.gymcrmsystemspringboot.repository.TrainingRepository;
import com.epam.gymcrmsystemspringboot.service.GeneratorService;
import com.epam.gymcrmsystemspringboot.service.serviceImpl.TraineeServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.*;

import static com.epam.gymcrmsystemspringboot.parametrs.MockData.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * @author Arpi Khachatryan
 * @date 21.05.2024
 */

@ExtendWith(MockitoExtension.class)
public class TraineeServiceImplTest {

    @InjectMocks
    private TraineeServiceImpl traineeService;

    @Mock
    private GeneratorService generatorService;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private TraineeRepository traineeRepository;

    @Mock
    private TrainingRepository trainingRepository;

    @Mock
    private TrainerRepository trainerRepository;

    @Mock
    private TraineeMapper traineeMapper;

    @Mock
    private TrainerMapper trainerMapper;

    @Mock
    private TrainingMapper trainingMapper;

    @Test
    public void testCreateTrainee() {
        TraineeRequestDTO traineeCreationDTO = getTraineeRequestDTO();
        Trainee trainee = new Trainee();
        trainee.setFirstName(getTraineeUpdateRequestDTO().getFirstName());
        trainee.setLastName(getTraineeUpdateRequestDTO().getLastName());
        trainee.setAddress(getTraineeUpdateRequestDTO().getAddress());
        trainee.setDateOfBirth(getTraineeUpdateRequestDTO().getDateOfBirth());
        trainee.setActive(true);

        when(generatorService.calculateBaseUserName(anyString(), anyString())).thenReturn("firstName.lastName");
        when(generatorService.generateUniqueUserName(anyString())).thenReturn("firstName.lastName");
        when(generatorService.generateRandomPassword()).thenReturn("password1234");
        when(passwordEncoder.encode(anyString())).thenReturn("hashedPassword1234");
        when(traineeMapper.traineeToTraineeResponseDto(any(Trainee.class))).thenReturn(getTraineeResponseDTO());
        when(traineeRepository.save(any(Trainee.class))).thenReturn(trainee);
        TraineeResponseDTO createdTraineeResponse = traineeService.createTrainee(traineeCreationDTO, "transactionId");

        assertEquals("firstName.lastName", createdTraineeResponse.getUsername());
        assertEquals("password1234", createdTraineeResponse.getPassword());
    }

    @Test
    public void testChangePassword() {
        Trainee trainee = getTrainee();
        trainee.setPassword("oldPassword");

        when(traineeRepository.findById(1L)).thenReturn(Optional.of(trainee));
        when(traineeRepository.save(any(Trainee.class))).thenReturn(trainee);
        traineeService.changePassword(getPasswordChangeDTO(), "transactionId");

        assertEquals("newPassword", trainee.getPassword());
    }

    @Test
    public void testUpdateTrainee() {
        TraineeUpdateRequestDTO updateRequestDTO = getTraineeUpdateRequestDTO();
        Trainee existingTrainee = getTrainee();
        existingTrainee.setFirstName(updateRequestDTO.getFirstName());
        TraineeUpdateResponseDTO expectedResponseDTO = getTraineeUpdateResponseDTO();

        when(traineeRepository.findByUsername(updateRequestDTO.getUsername())).thenReturn(Optional.of(existingTrainee));
        when(traineeRepository.save(any(Trainee.class))).thenReturn(existingTrainee);
        when(traineeMapper.traineeToTraineeUpdateResponseDTO(any(Trainee.class))).thenReturn(expectedResponseDTO);
        TraineeUpdateResponseDTO actualResponseDTO = traineeService.updateTrainee(updateRequestDTO, "transactionId");

        assertEquals(expectedResponseDTO, actualResponseDTO);
    }

    @Test
    public void testActivateDe_ActivateTrainee() {
        UserActivationDTO userActivationDTO = getUserActivationDTO();
        Trainee trainee = getTrainee();
        trainee.setActive(true);

        when(traineeRepository.findByUsername(userActivationDTO.getUsername())).thenReturn(Optional.of(trainee));
        when(traineeRepository.save(any(Trainee.class))).thenReturn(trainee);
        traineeService.activateDeactivateTrainee(userActivationDTO, "transactionId");

        assertFalse(trainee.isActive());
        verify(traineeRepository, times(1)).save(trainee);
    }

    @Test
    public void testDeleteTrainee() {
        String username = "firstName.lastName";
        Trainee trainee = getTrainee();

        when(traineeRepository.findByUsername(username)).thenReturn(Optional.of(trainee));
        traineeService.deleteTrainee(username, "transactionId");

        verify(traineeRepository, times(1)).delete(trainee);
    }

    @Test
    public void testGetTrainee() {
        TraineeGetResponseDTO responseDTO = getTraineeGetResponseDTO();
        Trainee trainee = getTrainee();

        when(traineeRepository.findByUsernameWithTrainers("firstName.lastName")).thenReturn(Optional.of(trainee));
        when(traineeMapper.traineeToTraineeGetResponseDTO(trainee)).thenReturn(responseDTO);
        TraineeGetResponseDTO result = traineeService.getTrainee("firstName.lastName", "transactionId");

        assertNotNull(result);
        assertTrue(result.isActive());
        verify(traineeRepository, times(1)).findByUsernameWithTrainers("firstName.lastName");
        verify(traineeMapper, times(1)).traineeToTraineeGetResponseDTO(trainee);
    }

    @Test
    public void testGetUnassignedTrainersByTraineeUsername() {
        String traineeUsername = "firstName.lastName";
        String transactionId = "transactionId";
        List<Trainer> unassignedTrainers = Arrays.asList(
                getTrainerData("firstName.lastName", "firstName", "lastName", "Fitness"),
                getTrainerData("firstName.lastName1", "firstName", "lastName", "Yoga")
        );
        List<TrainerDTO> trainerDTOs = Arrays.asList(
                getTrainerDTOData("firstName.lastName2", "firstName", "lastName", "Fitness"),
                getTrainerDTOData("firstName.lastName3", "firstName", "lastName", "Yoga")
        );

        when(trainerRepository.findUnassignedTrainersByTraineeUsername(traineeUsername)).thenReturn(unassignedTrainers);
        when(trainerMapper.trainerListToTrainerDTOList(unassignedTrainers)).thenReturn(trainerDTOs);
        List<TrainerDTO> result = traineeService.getUnassignedTrainersByTraineeUsername(traineeUsername, transactionId);

        assertNotNull(result);
        assertEquals(2, result.size());
        assertEquals("firstName.lastName2", result.get(0).getUsername());
        assertEquals("firstName.lastName3", result.get(1).getUsername());
        verify(trainerRepository, times(1)).findUnassignedTrainersByTraineeUsername(traineeUsername);
        verify(trainerMapper, times(1)).trainerListToTrainerDTOList(unassignedTrainers);
    }

    @Test
    public void testUpdateTraineeTrainers() {
        String traineeUsername = "firstName.lastName";
        String transactionId = "transactionId";
        Trainee trainee = getTrainee1Data(traineeUsername);
        Trainer trainer1 = getTrainerData("firstName.lastName");
        Trainer trainer2 = getTrainerData("firstName.lastName1");
        List<Trainer> trainers = Arrays.asList(trainer1, trainer2);
        List<TrainerDTO> trainerDTOs = Arrays.asList(getTrainerDTOData("firstName.lastName"), getTrainerDTOData("firstName.lastName1"));
        TraineeTrainersUpdateRequestDTO requestDTO = TraineeTrainersUpdateRequestDTO.builder()
                .username(traineeUsername)
                .trainerUpdateDTOList(Arrays.asList(
                        TrainerUpdateDTO.builder().username("firstName.lastName").build(),
                        TrainerUpdateDTO.builder().username("firstName.lastName1").build()
                ))
                .build();

        when(traineeRepository.findByUsername(traineeUsername)).thenReturn(Optional.of(trainee));
        when(trainerRepository.findAllByUsernameIn(anyList())).thenReturn(trainers);
        when(trainerMapper.trainerListToTrainerDTOList(trainers)).thenReturn(trainerDTOs);
        List<TrainerDTO> result = traineeService.updateTraineeTrainers(requestDTO, transactionId);

        assertNotNull(result);
        assertEquals(2, result.size());
        assertEquals("firstName.lastName", result.get(0).getUsername());
        assertEquals("firstName.lastName1", result.get(1).getUsername());
        verify(traineeRepository, times(1)).findByUsername(traineeUsername);
        verify(trainerRepository, times(1)).findAllByUsernameIn(anyList());
        verify(trainerMapper, times(1)).trainerListToTrainerDTOList(trainers);
        verify(traineeRepository, times(1)).save(trainee);
    }

    @Test
    public void testGetTraineeTrainingsByCriteria() {
        Training training = getTrainingData("Training", new Date(), 40);
        List<Training> trainings = List.of(training);
        TraineeTrainingResponseDTO trainingDTO = getTrainingDTOData("Training", "trainerName", "typeName", new Date(), 40);
        List<TraineeTrainingResponseDTO> trainingDTOs = Collections.singletonList(trainingDTO);

        when(trainingRepository.findAll(any(Specification.class))).thenReturn(trainings);
        when(trainingMapper.trainingToTraineeTrainingResponseDTO(trainings)).thenReturn(trainingDTOs);
        List<TraineeTrainingResponseDTO> result = traineeService.getTraineeTrainingsByCriteria("firstName.lastName", new Date(), new Date(), "trainerName", "typeName", "transactionId");

        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals(training.getName(), result.get(0).getTrainingName());
        verify(trainingRepository, times(1)).findAll(any(Specification.class));
        verify(trainingMapper, times(1)).trainingToTraineeTrainingResponseDTO(trainings);
    }
}