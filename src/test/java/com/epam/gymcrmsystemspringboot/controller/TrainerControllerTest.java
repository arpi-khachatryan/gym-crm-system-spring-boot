package com.epam.gymcrmsystemspringboot.controller;

import com.epam.gymcrmsystemspringboot.dto.general.PasswordChangeDTO;
import com.epam.gymcrmsystemspringboot.dto.general.UserActivationDTO;
import com.epam.gymcrmsystemspringboot.dto.trainer.TrainerGetResponseDTO;
import com.epam.gymcrmsystemspringboot.dto.trainer.TrainerRequestDTO;
import com.epam.gymcrmsystemspringboot.dto.trainer.TrainerUpdateRequestDTO;
import com.epam.gymcrmsystemspringboot.dto.trainer.TrainerUpdateResponseDTO;
import com.epam.gymcrmsystemspringboot.dto.training.TrainerTrainingResponseDTO;
import com.epam.gymcrmsystemspringboot.entity.Trainee;
import com.epam.gymcrmsystemspringboot.entity.Trainer;
import com.epam.gymcrmsystemspringboot.entity.Training;
import com.epam.gymcrmsystemspringboot.entity.TrainingType;
import com.epam.gymcrmsystemspringboot.repository.TraineeRepository;
import com.epam.gymcrmsystemspringboot.repository.TrainerRepository;
import com.epam.gymcrmsystemspringboot.repository.TrainingRepository;
import com.epam.gymcrmsystemspringboot.repository.TrainingTypeRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Arpi Khachatryan
 * @date 18.06.2024
 */

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc(addFilters = false)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TrainerControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TrainingTypeRepository trainingTypeRepository;

    @Autowired
    private TraineeRepository traineeRepository;

    @Autowired
    private TrainerRepository trainerRepository;

    @Autowired
    private TrainingRepository trainingRepository;

    private Trainer trainer;

    private Training training;
    private Trainee trainee;

    private TrainingType trainingType;

    @BeforeEach
    void setUp() {
        trainingType = trainingTypeRepository.save(TrainingType.builder()
                .name("Fitness")
                .build());

        trainer = new Trainer();
        trainer.setUsername("firstName.lastName1");
        trainer.setFirstName("firstName");
        trainer.setLastName("lastName");
        trainer.setPassword("password1234");
        trainer.setActive(true);
        trainer.setTrainingType(trainingType);
        trainerRepository.save(trainer);

        trainee = new Trainee();
        trainee.setUsername("firstName.lastName");
        trainee.setFirstName("firstName");
        trainee.setLastName("lastName");
        trainee.setPassword("password1234");
        trainee.setDateOfBirth(new Date());
        trainee.setAddress("address");
        trainee.setActive(true);
        trainee.setTrainers(List.of(trainer));
        traineeRepository.save(trainee);

        training = trainingRepository.save(Training.builder()
                .trainingDate(new Date())
                .duration(60)
                .name("trainingName")
                .trainer(trainer)
                .trainee(trainee)
                .trainingType(trainingType)
                .build());
    }

    @AfterEach
    void tearDown() {
        trainingRepository.deleteAll();
        trainerRepository.deleteAll();
        traineeRepository.deleteAll();
        trainingTypeRepository.deleteAll();
    }

    @Test
    public void testCreateTrainer() throws Exception {
        TrainerRequestDTO requestDTO = TrainerRequestDTO.builder()
                .firstName("newFirstName")
                .lastName("newLastName")
                .trainingTypeName("Pilates")
                .build();

        mockMvc.perform(MockMvcRequestBuilders.post("/trainers")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(requestDTO)))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.password").exists());
    }

    @Test
    public void testChangePassword() throws Exception {
        PasswordChangeDTO requestDTO = PasswordChangeDTO.builder()
                .userId(trainer.getId())
                .oldPassword(trainer.getPassword())
                .newPassword("newPassword123")
                .build();

        mockMvc.perform(MockMvcRequestBuilders.post("/trainers/change-password")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(requestDTO)))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testUpdateTrainer() throws Exception {
        TrainerUpdateRequestDTO requestDTO = TrainerUpdateRequestDTO.builder()
                .username(trainer.getUsername())
                .firstName(trainer.getFirstName())
                .lastName(trainer.getLastName())
                .specialization(trainer.getTrainingType().getName())
                .isActive(true)
                .build();

        TrainerUpdateResponseDTO mockResponse = TrainerUpdateResponseDTO.builder()
                .username(requestDTO.getUsername())
                .firstName(requestDTO.getFirstName())
                .lastName(requestDTO.getLastName())
                .specialization(requestDTO.getSpecialization())
                .isActive(requestDTO.getIsActive())
                .trainees(Collections.emptyList())
                .build();

        mockMvc.perform(MockMvcRequestBuilders.put("/trainers")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(requestDTO)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").value(requestDTO.getUsername()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value(requestDTO.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value(requestDTO.getLastName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.specialization").value(requestDTO.getSpecialization()));
    }

    @Test
    public void testActivateDeactivateTrainer() throws Exception {
        UserActivationDTO requestDTO = UserActivationDTO.builder()
                .username(trainer.getUsername())
                .isActive(false)
                .build();

        mockMvc.perform(MockMvcRequestBuilders.patch("/trainers/activate-deactivate")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(requestDTO)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("Trainer with username " + trainer.getUsername() + " activated/deactivated successfully."));
    }

    @Test
    public void testGetTrainer() throws Exception {
        TrainerGetResponseDTO responseDTO = TrainerGetResponseDTO.builder()
                .firstName(trainer.getFirstName())
                .lastName(trainer.getLastName())
                .specialization(trainer.getTrainingType().getName())
                .isActive(true)
                .trainees(Collections.emptyList())
                .build();

        mockMvc.perform(MockMvcRequestBuilders.get("/trainers/username/{username}", trainer.getUsername())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value(responseDTO.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value(responseDTO.getLastName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.specialization").value(responseDTO.getSpecialization()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.trainees").isEmpty());
    }

    @Test
    public void testGetTrainerTrainings() throws Exception {
        Training trainingForTrainer = trainingRepository.save(Training.builder()
                .trainingDate(new Date())
                .duration(60)
                .name("trainingName")
                .trainer(trainer)
                .trainee(trainee)
                .trainingType(trainingType)
                .build());
        trainingRepository.save(trainingForTrainer);

        TrainerTrainingResponseDTO trainingResponseDTO = TrainerTrainingResponseDTO.builder()
                .trainingName(trainingForTrainer.getName())
                .trainingDate(trainingForTrainer.getTrainingDate())
                .trainingTypeName(trainingForTrainer.getTrainingType().getName())
                .trainingDuration(trainingForTrainer.getDuration())
                .traineeName(trainingForTrainer.getTrainee().getUsername())
                .build();

        mockMvc.perform(MockMvcRequestBuilders.get("/trainers/{username}/trainings/search", trainer.getUsername())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].trainingName").value(trainingResponseDTO.getTrainingName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].trainingDate").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].trainingTypeName").value(trainingResponseDTO.getTrainingTypeName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].trainingDuration").value(trainingResponseDTO.getTrainingDuration()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].traineeName").value(trainingResponseDTO.getTraineeName()));
    }
}