package com.epam.gymcrmsystemspringboot.controller;

import com.epam.gymcrmsystemspringboot.dto.general.PasswordChangeDTO;
import com.epam.gymcrmsystemspringboot.dto.general.UserActivationDTO;
import com.epam.gymcrmsystemspringboot.dto.trainee.TraineeGetResponseDTO;
import com.epam.gymcrmsystemspringboot.dto.trainee.TraineeRequestDTO;
import com.epam.gymcrmsystemspringboot.dto.trainee.TraineeTrainersUpdateRequestDTO;
import com.epam.gymcrmsystemspringboot.dto.trainee.TraineeUpdateRequestDTO;
import com.epam.gymcrmsystemspringboot.dto.trainer.TrainerDTO;
import com.epam.gymcrmsystemspringboot.dto.trainer.TrainerUpdateDTO;
import com.epam.gymcrmsystemspringboot.dto.training.TraineeTrainingResponseDTO;
import com.epam.gymcrmsystemspringboot.entity.Trainee;
import com.epam.gymcrmsystemspringboot.entity.Trainer;
import com.epam.gymcrmsystemspringboot.entity.Training;
import com.epam.gymcrmsystemspringboot.entity.TrainingType;
import com.epam.gymcrmsystemspringboot.repository.TraineeRepository;
import com.epam.gymcrmsystemspringboot.repository.TrainerRepository;
import com.epam.gymcrmsystemspringboot.repository.TrainingRepository;
import com.epam.gymcrmsystemspringboot.repository.TrainingTypeRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Arpi Khachatryan
 * @date 18.06.2024
 */

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc(addFilters = false)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TraineeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TrainingTypeRepository trainingTypeRepository;

    @Autowired
    private TraineeRepository traineeRepository;

    @Autowired
    private TrainerRepository trainerRepository;

    @Autowired
    private TrainingRepository trainingRepository;

    private Trainer trainer;

    private Training training;
    private Trainee trainee;

    private TrainingType trainingType;

    @BeforeEach
    void setUp() {
        trainingType = trainingTypeRepository.save(TrainingType.builder()
                .name("Fitness")
                .build());

        trainer = new Trainer();
        trainer.setUsername("firstName.lastName1");
        trainer.setFirstName("firstName");
        trainer.setLastName("lastName");
        trainer.setPassword("password1234");
        trainer.setActive(true);
        trainer.setTrainingType(trainingType);
        trainerRepository.save(trainer);

        trainee = new Trainee();
        trainee.setUsername("firstName.lastName");
        trainee.setFirstName("firstName");
        trainee.setLastName("lastName");
        trainee.setPassword("password1234");
        trainee.setDateOfBirth(new Date());
        trainee.setAddress("address");
        trainee.setActive(true);
        trainee.setTrainers(List.of(trainer));
        traineeRepository.save(trainee);

        training = trainingRepository.save(Training.builder()
                .trainingDate(new Date())
                .duration(60)
                .name("trainingName")
                .trainer(trainer)
                .trainee(trainee)
                .trainingType(trainingType)
                .build());
    }

    @AfterEach
    void tearDown() {
        trainingRepository.deleteAll();
        trainerRepository.deleteAll();
        traineeRepository.deleteAll();
        trainingTypeRepository.deleteAll();
    }

    @Test
    public void testCreateTrainee() throws Exception {
        TraineeRequestDTO requestDTO = TraineeRequestDTO.builder()
                .firstName("firstName")
                .lastName("lastName")
                .dateOfBirth(new Date())
                .address("address")
                .trainingIds(Set.of(training.getId()))
                .trainerId(trainer.getId())
                .build();

        mockMvc.perform(MockMvcRequestBuilders.post("/trainees")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(requestDTO)))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.password").exists());
    }

    @Test
    public void testChangePassword() throws Exception {
        PasswordChangeDTO requestDTO = PasswordChangeDTO.builder()
                .userId(trainee.getId())
                .oldPassword(trainee.getPassword())
                .newPassword("newPassword123")
                .build();

        mockMvc.perform(MockMvcRequestBuilders.post("/trainees/change-password")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(requestDTO)))
                .andExpect(status().isOk());
    }

    @Test
    public void testUpdateTrainee() throws Exception {
        TraineeUpdateRequestDTO requestDTO = TraineeUpdateRequestDTO.builder()
                .username(trainee.getUsername())
                .firstName(trainee.getFirstName())
                .lastName(trainee.getLastName())
                .address(trainee.getAddress())
                .isActive(true)
                .build();

        mockMvc.perform(MockMvcRequestBuilders.put("/trainees")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(requestDTO)))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").value(requestDTO.getUsername()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value(requestDTO.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value(requestDTO.getLastName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.address").value(requestDTO.getAddress()));
    }

    @Test
    public void testActivateDeactivateTrainee() throws Exception {
        UserActivationDTO requestDTO = UserActivationDTO.builder()
                .username(trainee.getUsername())
                .isActive(true)
                .build();

        mockMvc.perform(MockMvcRequestBuilders.patch("/trainees/activate-deactivate")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(requestDTO)))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("Trainee with username " + trainee.getUsername() + " activated/deactivated successfully."));
    }

    @Test
    public void testDeleteTrainee() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/trainees/{username}", trainee.getUsername()))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetTrainee() throws Exception {
        TraineeGetResponseDTO traineeResponse = TraineeGetResponseDTO.builder()
                .firstName(trainee.getFirstName())
                .lastName(trainee.getLastName())
                .dateOfBirth(trainee.getDateOfBirth())
                .address(trainee.getAddress())
                .isActive(true)
                .build();

        mockMvc.perform(get("/trainees/username/{username}", trainee.getUsername())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value(traineeResponse.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value(traineeResponse.getLastName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.dateOfBirth").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.address").value(traineeResponse.getAddress()));
    }

    @Test
    public void testGetTraineeTrainings() throws Exception {
        Training trainingForTrainee = trainingRepository.save(Training.builder()
                .trainingDate(new Date())
                .duration(60)
                .name("trainingName")
                .trainer(trainer)
                .trainee(trainee)
                .trainingType(trainingType)
                .build());
        trainingRepository.save(trainingForTrainee);
        trainee.setTrainings(List.of(trainingForTrainee));
        traineeRepository.save(trainee);

        List<TraineeTrainingResponseDTO> expectedResponse = Collections.singletonList(
                TraineeTrainingResponseDTO.builder()
                        .trainingName(trainee.getTrainings().get(0).getName())
                        .trainingDate(trainee.getTrainings().get(0).getTrainingDate())
                        .trainingTypeName(trainee.getTrainings().get(0).getTrainingType().getName())
                        .trainingDuration(trainee.getTrainings().get(0).getDuration())
                        .trainerName(trainee.getTrainings().get(0).getTrainer().getUsername()).build()
        );

        mockMvc.perform(get("/trainees/{username}/trainings/search", trainee.getUsername())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].trainingName").value(expectedResponse.get(0).getTrainingName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].trainingTypeName").value(expectedResponse.get(0).getTrainingTypeName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].trainingDuration").value(expectedResponse.get(0).getTrainingDuration()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].trainerName").value(expectedResponse.get(0).getTrainerName()));
    }

    @Test
    public void testGetUnassignedTrainers() throws Exception {
        List<TrainerDTO> trainerDTOS = Collections.singletonList(
                TrainerDTO.builder()
                        .username(trainer.getUsername())
                        .firstName(trainer.getFirstName())
                        .lastName(trainer.getLastName())
                        .specialization(trainer.getTrainingType().toString())
                        .build()
        );

        mockMvc.perform(MockMvcRequestBuilders.get("/trainees/{username}/unassigned-trainers", trainee.getUsername())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].username").value(trainerDTOS.get(0).getUsername()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].firstName").value(trainerDTOS.get(0).getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].lastName").value(trainerDTOS.get(0).getLastName()));
    }

    @Test
    public void testUpdateTraineeTrainers() throws Exception {
        TraineeTrainersUpdateRequestDTO requestDTO = TraineeTrainersUpdateRequestDTO.builder()
                .username(trainee.getUsername())
                .trainerUpdateDTOList(Collections.singletonList(
                        TrainerUpdateDTO.builder()
                                .username(trainer.getUsername())
                                .build())).build();

        List<TrainerDTO> trainerDTOS = Collections.singletonList(
                TrainerDTO.builder()
                        .username(trainer.getUsername())
                        .firstName(trainer.getFirstName())
                        .lastName(trainer.getLastName())
                        .specialization(trainer.getTrainingType().getName())
                        .build()
        );

        mockMvc.perform(MockMvcRequestBuilders.put("/trainees/update-trainers")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(requestDTO)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].username").value(trainerDTOS.get(0).getUsername()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].firstName").value(trainerDTOS.get(0).getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].lastName").value(trainerDTOS.get(0).getLastName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].specialization").value(trainerDTOS.get(0).getSpecialization()));
    }
}