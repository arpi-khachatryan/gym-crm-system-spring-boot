package com.epam.gymcrmsystemspringboot.controller;

import com.epam.gymcrmsystemspringboot.dto.training.TrainingRequestDTO;
import com.epam.gymcrmsystemspringboot.entity.Trainee;
import com.epam.gymcrmsystemspringboot.entity.Trainer;
import com.epam.gymcrmsystemspringboot.entity.TrainingType;
import com.epam.gymcrmsystemspringboot.repository.TraineeRepository;
import com.epam.gymcrmsystemspringboot.repository.TrainerRepository;
import com.epam.gymcrmsystemspringboot.repository.TrainingRepository;
import com.epam.gymcrmsystemspringboot.repository.TrainingTypeRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Date;

/**
 * @author Arpi Khachatryan
 * @date 18.06.2024
 */

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc(addFilters = false)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TrainingControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TrainingRepository trainingRepository;

    @Autowired
    private TrainingTypeRepository trainingTypeRepository;

    @Autowired
    private TraineeRepository traineeRepository;

    @Autowired
    private TrainerRepository trainerRepository;

    private Trainee trainee;

    private Trainer trainer;

    private TrainingType trainingType;

    @BeforeEach
    void setUp() {
        trainingType = trainingTypeRepository.save(TrainingType.builder()
                .name("trainingTypeName")
                .build());
        trainingTypeRepository.save(trainingType);

        trainee = new Trainee();
        trainee.setUsername("firstName.lastNameTraining");
        trainee.setFirstName("firstName");
        trainee.setLastName("lastName");
        trainee.setPassword("password1234");
        trainee.setDateOfBirth(new Date());
        trainee.setAddress("address");
        trainee.setActive(true);
        traineeRepository.save(trainee);

        trainer = new Trainer();
        trainer.setUsername("firstName.lastNameTraining1");
        trainer.setFirstName("firstName");
        trainer.setLastName("lastName");
        trainer.setPassword("password1234");
        trainer.setActive(true);
        trainer.setTrainingType(trainingType);
        trainerRepository.save(trainer);
    }

    @AfterEach
    void tearDown() {
        trainingRepository.deleteAll();
        trainerRepository.deleteAll();
        traineeRepository.deleteAll();
        trainingTypeRepository.deleteAll();
    }

    @Test
    public void testCreateTraining() throws Exception {
        TrainingRequestDTO request = new TrainingRequestDTO();
        request.setTraineeUsername(trainee.getUsername());
        request.setTrainerUsername(trainer.getUsername());
        request.setTrainingTypeName(trainingType.getName());
        request.setTrainingName("newTrainingName");
        request.setTrainingDate(new Date());
        request.setTrainingDuration(60);
        ObjectMapper objectMapper = new ObjectMapper();
        String requestBody = objectMapper.writeValueAsString(request);

        mockMvc.perform(MockMvcRequestBuilders.post("/trainings")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}