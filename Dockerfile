FROM openjdk:17-jdk-slim
WORKDIR /app
COPY target/gym-crm-system-spring-boot-0.0.1-SNAPSHOT.jar app.jar
EXPOSE 8081
ENTRYPOINT ["java", "-jar", "/app/app.jar"]
#ENTRYPOINT ["java", "-jar", "-Dspring.profiles.active=docker", "/app/app.jar"]