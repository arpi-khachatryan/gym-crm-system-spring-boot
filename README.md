## [Gym CRM System](https://gitlab.com/arpi-khachatryan/gym-crm-system-spring-boot)
Welcome to the Gym CRM System project repository! This project builds a CRM system customized for gyms. It uses Spring Boot for a strong backend foundation and MySQL to store data long-term.

### Features

- **Swagger**: [API documentation and testing](https://swagger.io/). Easily explore and test APIs.
- **Docker**: [Containerization for deployment](https://www.docker.com/).
- **MySQL**: [Database management system](https://www.mysql.com/).
- **PostgreSQL**: [Database management system for Docker](https://www.postgresql.org/). Used for Docker containerization.
- **Mappers**: [MapStruct](https://mapstruct.org/). Streamlines data transfer between system layers through object mapping.
- **Metrics (Prometheus)**: [Monitors system health](https://prometheus.io/).
- **Health Indicators**: Custom health indicators to monitor critical aspects of the application.
- **Unit Tests**: Tests to ensure reliability and maintainability of the codebase.
- **Integration Tests**: Tests to validate system components and interactions.
- **Spring Security**: Includes brute force protection and CORS policy.
- **JWT Authentication**: Secures endpoints using [JSON Web Tokens](https://jwt.io/).